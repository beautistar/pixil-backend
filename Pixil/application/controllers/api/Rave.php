<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rave extends CI_Controller {     

    function __construct(){

        parent::__construct();         
        $this->load->database();
        $this->load->model('api/rave_model', 'rave_model');             
    }
    
    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    
    
    function create_rave() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');
        $user_name = $this->input->post('user_name');
        $name = $this->input->post('name');
        $mode = $this->input->post('mode');
        $paticipant_capacity = $this->input->post('participant_capacity');
        $date = $this->input->post('date');
        $is_chargeable = $this->input->post('is_chargeable');
        $price = $this->input->post('price');
        $location_name = $this->input->post('location_name');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $feature = $this->input->post('feature');
        $description = $this->input->post('description');
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 40000,
            'max_height' => 40000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('icon_picture')) {

            // add rave
            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('owner_id' => $user_id,
                          'owner_name' => $user_name,
                          'name' => $name,
                          'mode' => $mode,
                          'participant_capacity' => $paticipant_capacity,
                          'location_name' => $location_name,
                          'latitude' => $latitude,
                          'longitude' => $longitude,
                          'feature' => $feature,
                          'date' => $date,
                          'price' => $price,
                          'is_chargeable' => $is_chargeable,
                          'description' => $description,
                          'icon_url' => $file_url,
                          'created_at' => date('Y-m-d h:m:s')                                                     
                          );
            
            // add location
            //$data = array('name' => $location_name,
            //          'latitude' => $latitude,
            //          'longitude' => $longitude,
            //          'created_at' => date('Y-m-d h:m:s'));
        
            //$this->rave_model->add_location($data);
            
            $result['rave_id'] = $this->rave_model->add_rave($data);             
            $result['icon_pic_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {
            $result['message'] = "Rave creation failed with icon picture upload.";
            $this->doRespond(203, $result);
            return;
        }
    }
    
    
    // get public raves
    function get_public_raves() {
        
        $result['rave_list'] =  $this->rave_model->get_public_raves();
        $this->doRespondSuccess($result);
    }
    
    // add location
    
    function add_location() {
        
        $data = array('name' => $this->input->post('name'),
                      'latitude' => $this->input->post('latitude'),
                      'longitude' => $this->input->post('longitude'),
                      'created_at' => date('Y-m-d h:m:s'));
        
        $this->rave_model->add_location($data);
        $this->doRespondSuccess(array());
    }
    
    // set to favorite location
    
    function set_favorite_location() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'location_id' => $this->input->post('location_id'));
        $this->rave_model->set_favorite_location($data);
        $this->doRespondSuccess(array());
        
    }
    
    // get favorite locations by user
    
    function get_favorite_location() {
        
        $response = array();
        $result = array();
        $user_id = $this->input->post('user_id');
        $latitude =  $this->input->post('latitude');
        $longitude =  $this->input->post('longitude');
        $locations = $this->rave_model->get_favorite_location($user_id);
        
        foreach ($locations as $location) {
            
            $one = array('id' => $location['id'],
                         'name' => $location['name'],
                         'latitude' => $location['latitude'],
                         'longitude' => $location['longitude'],
                         'distance' => $this->getDistance(
                            $latitude, $longitude, $location['latitude'], $location['longitude']))
                         ;
            array_push($result, $one);
        }
         
        $response['location_list'] = $result;
        $this->doRespondSuccess($response);
        
        
    }
    
    function getDistance(
          $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
        {
            // convert from degrees to radians
            $latFrom = deg2rad($latitudeFrom);
            $lonFrom = deg2rad($longitudeFrom);
            $latTo = deg2rad($latitudeTo);
            $lonTo = deg2rad($longitudeTo);

            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;

            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
            return $angle * $earthRadius;
        }
    
    // get recommended locations
    
    function get_recommended_location() {
        
        $distance = 500;
        
        $latitude =  $this->input->post('latitude');
        $longitude =  $this->input->post('longitude');
        $result['location_list'] = $this->rave_model
                                        ->get_recommended_location($distance, $latitude, $longitude);
        
        $this->doRespondSuccess($result);
    }
    
    // join to raves
    
    function join_rave() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'rave_id' => $this->input->post('rave_id'),
                      'joined_at' => date('Y-m-d'),
                      );
        
        $result = $this->rave_model->join_rave($data);
        if ($result == TRUE) {
            // increase rave participants count.
            $this->rave_model->increase_participants($this->input->post('rave_id'));
            
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Already joined to this rave.";
            $this->doRespond(205, $result);
        }        
    }
    
    
    // get user joined raves
    
    function get_joined_rave() {
        
        $user_id = $this->input->post('user_id');
        $result['rave_list'] =  $this->rave_model->get_joined_rave($user_id);
        $this->doRespondSuccess($result);
        
    }
    
    
    // add rave to user's wishlist
    
    function add_wishlist() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'rave_id' => $this->input->post('rave_id')
                      );
        
        $result = $this->rave_model->add_wishlist($data);
        if ($result == TRUE) {
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Rave is already in wishlist.";
            $this->doRespond(205, $result);
        }      
        
    }
    
    // get user wishlist raves
    
    function get_wishlist_rave() {
        
        $user_id = $this->input->post('user_id');
        $result['rave_list'] =  $this->rave_model->get_wishlist_rave($user_id);
        $this->doRespondSuccess($result);
        
    }
    
    // invite rave to his friends.
    
    function invite_rave() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'friend_id' => $this->input->post('friend_id'),
                      'rave_id' => $this->input->post('rave_id')
                      );
        
        $result = $this->rave_model->invite_rave($data);
        if ($result == TRUE) {
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Rave is already invited to this friend.";
            $this->doRespond(205, $result);
        }      
        
    }
    
    // add rave template
    // it need to be done by admin.
    // For test, adding here
    
    function add_template() {
        
        $data = array('name' => $this->input->post('name'),
                      'participant_capacity' => $this->input->post('participant_capacity'),
                      'is_chargeable' => $this->input->post('is_chargeable'),
                      'mode' => $this->input->post('mode')
                      );
        
        $result = $this->rave_model->add_template($data);
        if ($result == TRUE) {
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Template name is already exist.";
            $this->doRespond(205, $result);
        }        
        
    }
    
    // get recommended rave template created by admin.
    
    function get_template() {
        
        $result['template_list'] = $this->rave_model->get_template();
        $this->doRespondSuccess($result);
        
        
    }
    
    // add to favorite template
    
    function add_favorite_template() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'template_id' => $this->input->post('template_id')
                      );
        
        $result = $this->rave_model->add_favorite_template($data);
        if ($result == TRUE) {
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Template is already favorited.";
            $this->doRespond(205, $result);
        }
        
        
    }
    
    // get favorite rave template created by user.
    
    function get_favorite_template() {
        
        $user_id = $this->input->post('user_id');
        
        $result['template_list'] = $this->rave_model->get_favorite_template($user_id);
        $this->doRespondSuccess($result);
        
        
    }
    
    // add rave to IGO
    
    function add_igo_rave() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'rave_id' => $this->input->post('rave_id')
                      );
        
        $result = $this->rave_model->add_to_igo($data);
        if ($result == TRUE) {
            $this->doRespondSuccess(array()); 
        } else {
            $result['message'] = "Rave is already in IGO or you have full IGO upto 10.";
            $this->doRespond(205, $result);
        }        
    }
    
    // get IGO raves by user. IGO means raves that user planing to go
    
    function get_igo_rave() {
        
        $user_id = $this->input->post('user_id');
        
        $result['igo_rave_list'] = $this->rave_model->get_igo($user_id);
        $this->doRespondSuccess($result);
    }
    
    // get IDO raves by user. IGO is user created raves.
    
    function get_ido_rave() {
        
        $user_id = $this->input->post('user_id');
        
        $result['ido_rave_list'] = $this->rave_model->get_ido($user_id);
        $this->doRespondSuccess($result);
        
        
    }
    
    function get_igo_ido_rave() {
        
        $user_id = $this->input->post('user_id');
        
        $result['igo_rave_list'] = $this->rave_model->get_igo($user_id);
        $result['ido_rave_list'] = $this->rave_model->get_ido($user_id);
        $this->doRespondSuccess($result);
        
        
    }
    
    // Process of rave state tracking 5.3 ~
    // allow edit rave icon and description by admin
    function allow_edit() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->allow_edit($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // non allow edit rave icon and description by admin
    function not_allow_edit() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->not_allow_edit($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // edit rave
    function edit_rave() {
        
        $result = array();
        
        $rave_id = $this->input->post('rave_id');
        $description = $this->input->post('description'); 
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 4000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('icon_picture')) {

            // add rave
            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('description' => $description,
                          'icon_url' => $file_url,
                          'updated_at' => date('Y-m-d h:m:s')                                                     
                          );
            
            $this->rave_model->edit_rave($rave_id, $data);             
            $result['icon_pic_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {
            $result['message'] = "Rave editing is failed with icon picture upload.";
            $this->doRespond(203, $result);
            return;
        }
        
        
    }
    
    // allows admin to hide his rave from people he doesn’t want to see
    function allow_hide() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->allow_hide($rave_id);
        $this->doRespondSuccess(array());
    }
    
    // Don't allows admin to hide his rave from people he doesn’t want to see
    function not_allow_hide() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->not_allow_hide($rave_id);
        $this->doRespondSuccess(array());
    }
    
    // allows admin to send invitation to specific friends that he has inside the app
    function allow_invite() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->allow_invite($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // Do not allows admin to send invitation to specific friends that he has inside the app
    function not_allow_invite() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->not_allow_invite($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // allows admin to pay for the inside advertisement like Facebook Ads 
    //(adjust by region, segment or age)
    function allow_promote() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->allow_promote($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // Don't allows admin to pay for the inside advertisement like Facebook Ads 
    //(adjust by region, segment or age)
    function not_allow_promote() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->not_allow_promote($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // admin to see rave chat
    function allow_see_chat() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->allow_see_chat($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // Do not admin to see rave chat
    function not_allow_see_chat() {
        
        $rave_id = $this->input->post('rave_id');
        $this->rave_model->not_allow_see_chat($rave_id);
        $this->doRespondSuccess(array());
        
    }
    
    // get joined history  (for statics graph)
    
    function get_statics() {
        
        $rave_id = $this->input->post('rave_id');
        $result['statics_info'] =  $this->rave_model->get_statics($rave_id);
        $this->doRespondSuccess($result);
    }

    // get user created rave list (for static on profile page)

    function get_my_raves() {

        $user_id = $this->input->post('user_id');
        $result['rave_list'] =  $this->rave_model->get_my_raves($user_id);
        $this->doRespondSuccess($result);


    }
    
    
}

?>
