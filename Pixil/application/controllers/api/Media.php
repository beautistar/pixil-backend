<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {     

    function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->model('api/media_model', 'media_model');
        $this->load->model('api/user_model', 'user_model');
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }
    
    
    // upload post picture or media video
    function post_media() {
        
        $user_id = $this->input->post('user_id');
        $user_name = $this->input->post('user_name');
        $type = $this->input->post('type');
        $description = $this->input->post('description');
        $hashtags = $this->input->post('hashtags'); //life_dog_cat_...
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 40000,
            'max_height' => 40000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {
            
            // add or update hashtag data
            $arr_hashtags = explode('_', $hashtags);
            foreach($arr_hashtags as $hashtag) {                
                
                $created_at =  date('Y-m-d h:m:s');
                $this->media_model->add_hashtag($user_id, $hashtag, $created_at);

            }

            // add media
            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('user_id' => $user_id,
                          'content' => $file_url,
                          'type' => $type,
                          'description' => $description,
                          'hashtag' => $hashtags,
                          'user_name' => $user_name,
                          'created_at' => date('Y-m-d h:m:s')                                                     
                          );
            $this->media_model->add_media($data);
            
            $result['file_url'] = $file_url;            
                      
            $this->doRespondSuccess($result);

        } else {
            $result['message'] = "Media upload faied.";
            $this->doRespond(203, $result);
            return;
        }
    }

    /**
    * get post media
    * 
    */
    
    
    function get_post_media() {
        
        $result = array();
        $t_result = array();
        $media_type = $_POST['type']; // video or picture or all
        $type = 'media'; 
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        
        $query_result = $this->media_model->get_post_media($page, $media_type);
         
        if ($query_result == 0) {
            $result['media_list'] = array();
            $this->doRespond(202, $result);
            return;
        }
        
        foreach ($query_result as $one) {
            
            $one['comment_count'] = $this->media_model->get_comment_count($one['id'], $type);
            $one['like_count'] = $this->media_model->get_like_count($one['id'], $type);
            
            array_push($t_result, $one);
        }
        
        $result['media_list'] =  $t_result;
        $this->doRespondSuccess($result);         
    }
    
    // leave comment to media content.
    function leave_comment() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'media_id' => $this->input->post('media_id'),
                      'comment' => $this->input->post('comment'),
                      'type' => $this->input->post('type'),
                      'created_at' => date('Y-m-d h:m:s'));
                      
        $result['comment_id'] = $this->media_model->add_comment($data);
        $this->doRespondSuccess($result) ;
    }
    
    // like media content to his friends.
    function like_to_friends() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'media_id' => $this->input->post('media_id'),
                      'created_at' => date('Y-m-d h:m:s'));
                      
        $this->media_model->like_to_friends($data);
        $this->doRespondSuccess(array()) ;
        
        
    }
    
    // like media content 
    function like_media() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'media_id' => $this->input->post('media_id'),
                      'type' => $this->input->post('type'));  
                      
        $result = $this->media_model->like_media($data);
        
        if ($result == false) {
            
            $message = "You already liked it";
            $this->doRespond(206, array('message' => $message));
        } else {
            
            $this->doRespondSuccess(array());
        }
        
        
        
    }
    
    // search by user's name
    
    function search_media_by_username() {
        
        $user_name = $this->input->post('user_name');
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        
        $result['media_list'] = $this->media_model->search_by_username($user_name, $page);
        
        $this->doRespondSuccess($result);
    }
    
    // search by hashtag
    
    function search_media_by_hashtag() {
        
        $hashtag = $this->input->post('hashtag');
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        
        $result['media_list'] = $this->media_model->search_by_hashtag($hashtag, $page);
        
        $this->doRespondSuccess($result);
    }
    
    // find hashtags
    
    function search_hashtags() {
        
        $hashtag_name = $this->input->post('name');
        
        $result['hashtag_list'] = $this->media_model->search_hashtags($hashtag_name);
        
        $this->doRespondSuccess($result);
    }
    
    // post text
    
    function post_text() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'text' => $this->input->post('text'),
                      'created_at' => date('Y-m-d h:m:s'));
                      
        $this->media_model->post_text($data);
        
        $this->doRespondSuccess(array());
        
        
    }
    
    // get post text list  of userself and their friend's text.
    
    function get_post_text() {
        
        $t_result = array();
        
        $type = 'text';
        $user_id = $this->input->post('user_id');
        
        $query_result = $this->media_model->get_post_text($user_id);
        
        foreach ($query_result as $one) {
            
            $comment_count = $this->media_model->get_comment_count($one['id'], $type);
            $like_count = $this->media_model->get_like_count($one['id'], $type);
            $one['comment_count'] = $comment_count;
            $one['like_count'] = $like_count;
            
            array_push($t_result, $one);
        }
        
        $result['text_list'] = $t_result;
        
        $this->doRespondSuccess($result); 
        
    }
    
    // get all comments in a post
    function get_comment() {
        
        $media_id = $this->input->post('media_id');
        $type = $this->input->post('type');
        
        $result['comment_list'] = $this->media_model->get_comment($media_id, $type);
        
        $this->doRespondSuccess($result);
    }
    
    // delete comment
    
    function delete_comment() {
        
        $comment_id =  $this->input->post('comment_id');
        $this->media_model->delete_comment($comment_id);
        $this->doRespondSuccess(array());
    }
    
    // get all post text
    
    function get_all_post_text() {
        
        $result = array();
        $t_result = array();
        $type = 'text'; 
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        
        $query_result = $this->media_model->get_all_post_text($page);
         
        if ($query_result == 0) {
            $result['text_list'] = array();
            $this->doRespond(202, $result);
            return;
        }
        
        foreach ($query_result as $one) {
            
            $one['comment_count'] = $this->media_model->get_comment_count($one['id'], $type);
            $one['like_count'] = $this->media_model->get_like_count($one['id'], $type);
            
            array_push($t_result, $one);
        }
        
        $result['text_list'] =  $t_result;
        $this->doRespondSuccess($result);         
    }
    

    
}

?>
