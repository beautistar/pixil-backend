<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {     

    function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->model('api/user_model', 'user_model');
        $this->load->model('api/media_model', 'media_model');
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }
    
    
    function gen_qrcode() {
        
        $time = explode(' ', microtime());
        $NameTime = substr($time[0], 2, 3);        
        $NameRand = mt_rand(0, 0xffff);
        $qr_code = $NameRand.$NameTime; 
        $qr_code = $qr_code * 10 / 10;
        $len = strlen($qr_code);        
        if($len < 10){
            for ($i = 0; $i < 8-$len; $i++)
               $qr_code .= mt_rand(0,9);
        }
        return $qr_code;       
        
    }     
    
    function login() {
        
        $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
                );
        $result = $this->user_model->login($data);
        
        if ($result == TRUE) {
            $user_data['user_data'] = array(
                'id' => $result['id'],
                'name' => $result['name'],
                'surname' => $result['surname'],
                'email' => $result['email'],
                'phone' => $result['phone'],
                'photo_url' => $result['photo_url'],
                'qr_code' => $result['qr_code'],
                'birthday' => $result['birthday']
            );
            
            $this->doRespondSuccess($user_data);
        }
        else{
            
            $message = "Invalid login";
            $this->doRespond(201, array('message' => $message));
        }
    }
    
    
    function check_exist() {
        
        $response = array();
        
        $param = $this->input->post('param');
        $type = $this->input->post('type');
        
        $result = $this->user_model->check_exist($param, $type);
        
        if ($result == TRUE) {
            
            $response['message'] = $type. " already eixst.";
            $this->doRespond(202, $response);
        
        } else {
            
            $this->doRespondSuccess($response);
            
        }
    }
    
    function register() {
        
        $result = array();
        
        $data = array('name' => $this->input->post('name'),
                      'surname' => $this->input->post('surname'),
                      'username' => $this->input->post('username'),
                      'email' => $this->input->post('email'),
                      'birthday' => $this->input->post('birthday'),
                      'phone' => $this->input->post('phone'),
                      'qr_code' => $this->gen_qrcode(),
                      'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                      'created_at' => date('Y-m-d h:m:s'),                           
                  );
        $id = $this->user_model->add_user($data);
                   
        $result['id'] = $id;
                    
        $this->doRespondSuccess($result);                 
        
    }
    
    
    // edit user information in profile page
    function edit_user() {
        
        $id = $this->input->post('id');
        
        $data = array('id' => $this->input->post('id'),
                      'name' => $this->input->post('name'),
                      'surname' => $this->input->post('surname'),
                      'username' => $this->input->post('username'),
                      'updated_at' => date('Y-m-d h:m:s')                           
        );
        
        $result = $this->user_model->edit_user($id, $data);
        
        $this->doRespondSuccess(array());
        
        
    }
    
    
    // upload user profile photo
    function upload_photo() {
        
        $id = $this->input->post('user_id');
        
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 40000,
            'max_height' => 40000,
            'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array(
                          'photo_url' => $file_url,
                          'updated_at' => date('Y-m-d h:m:s')                                                     
                      );
            $id = $this->user_model->update_photo($id, $data);
            $result['photo_url'] = $file_url;            
                      
            $this->doRespondSuccess($result);

        } else {
            $result['message'] = "Photo upload faied.";
            $this->doRespond(203, $result);
            return;
        }
    }
    
    /**
    * get post owner profile information
    * 
    */
    function get_user_profile() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');
        $target_id = $this->input->post('target_id');
        
        $user_data = $this->user_model->get_user($target_id);        
        
        $result['user_data'] = array(
                'id' => $user_data['id'],
                'name' => $user_data['name'],
                'surname' => $user_data['surname'],
                'username' => $user_data['username'],
                'email' => $user_data['email'],
                'phone' => $user_data['phone'],
                'qr_code' => $user_data['qr_code'],
                'photo_url' => $user_data['photo_url'],
                'birthday' => $user_data['birthday'],
                'is_friend' => $this->user_model->check_friend($user_id, $target_id)
            );
        
        $posts = $this->media_model->get_user_media($target_id);
            
        $result['post_data'] = $posts;        
            
        $this->doRespondSuccess($result);        
        
    }
    
    // search user by qr code.
    
    function get_user_by_qrcode() {
        
        $qr_code = $this->input->post('qr_code');
        
        $result = $this->user_model->get_user_by_qrcode($qr_code);
        
        if ($result == TRUE) {
            $user_data['user_data'] = array(
                'id' => $result['id'],
                'name' => $result['name'],
                'surname' => $result['surname'],
                'email' => $result['email'],
                'phone' => $result['phone'],
                'photo_url' => $result['photo_url'],
                'qr_code' => $result['qr_code'],
                'birthday' => $result['birthday']
            );
            
            $this->doRespondSuccess($user_data);
        }
        else{
            
            $message = "User with this QR code does not exist ";
            $this->doRespond(201, array('message' => $message));
        }
    }
    
    // add friend
    
    function add_friend() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'friend_id' => $this->input->post('friend_id'));
                      
        $result = $this->user_model->add_friend($data);
        
        if ($result == TRUE) {
            $this->doRespondSuccess(array());
            
        } else {
            $response = array('message' => "User is already friend.");
            $this->doRespond(204, $response);
        }
    }
    
    // get friend list by user
    
    function get_friends() {
        
        $user_id = $this->input->post('user_id');
        
        $result['friend_list'] = $this->user_model->get_freinds($user_id);
        
        $this->doRespondSuccess($result);
        
    } 
    
    // get all users
    
    function get_users() {
        
        $users = array();
        
        
        $user_id = $this->input->post('user_id');
        
        $q_result = $this->user_model->get_users($user_id);
        
        foreach ($q_result as $one) {
            
            $user = array(
                'id' => $one['id'],
                'name' => $one['name'],
                'surname' => $one['surname'],
                'username' => $one['username'],
                'email' => $one['email'],
                'phone' => $one['phone'],
                'photo_url' => $one['photo_url'],
                'qr_code' => $one['qr_code'],
                'birthday' => $one['birthday']);
                
            array_push($users, $user);
        }
        
        $result['user_list'] = $users;
        $this->doRespondSuccess($result);
        
        
    }
    
    // Search user by name
    
    function search_user_by_name() {
        
        
        $users = array();
        
        $name = $this->input->post('name');
        $user_id = $this->input->post('user_id');
        
        $q_result = $this->user_model->search_user_by_name($user_id, $name);
        
        foreach ($q_result as $one) {
            
            $user = array(
                'id' => $one['id'],
                'name' => $one['name'],
                'surname' => $one['surname'],
                'username' => $one['username'],
                'email' => $one['email'],
                'phone' => $one['phone'],
                'photo_url' => $one['photo_url'],
                'qr_code' => $one['qr_code'],
                'birthday' => $one['birthday']);
                
            array_push($users, $user);
        }
        
        $result['user_list'] = $users;
        $this->doRespondSuccess($result);
        
        
    }
    
    
    // check if target user is friend or not
    
    function check_friend() {
        
        $friend_id = $this->input->post('friend_id');
        $user_id = $this->input->post('user_id');       
        
        if ($this->user_model->check_friend($user_id, $friend_id) == true) {
            
            $result['result'] = "You are already friend";
            $this->doRespondSuccess($result);
            
        } else {
            
            $result['message'] = "You are not friend";
            $this->doRespond(208, $result);
            
            
        }
        
        
    }                              
    
}
?>
