<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Rave extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/rave_model', 'rave_model');
            
        }

        //----------------------------------------------------------------------
        //  Rave
        public function index(){
            $data['all_raves'] =  $this->rave_model->get_all_rave();            
            $data['title'] = 'Rave';
            $data['view'] = 'admin/rave/rave_list';
            $this->load->view('admin/layout', $data);
        }
        
        //----------------------------------------------------------------------
        //  Rave visible switch
        
        public function update_visible($id = 0) {
            
            $this->rave_model->update_visible($id);
            $this->session->set_flashdata('msg', 'Rave visiblity has been updated successfully!');
            redirect(base_url('admin/rave'));
        }
        
        //----------------------------------------------------------------------
        //  update rave chargeable or free
        
        public function update_chargeable($id = 0) {
            
            $this->rave_model->update_chargeable($id);
            $this->session->set_flashdata('msg', 'Rave chargeable has been updated successfully!');
            redirect(base_url('admin/rave'));
        }
        
        public function edit_rave($rave_id = 0) {
        
            if($this->input->post('submit')){
                
                $this->form_validation->set_rules('description', 'Description', 'trim|required');
                $this->form_validation->set_rules('price', 'Price', 'trim|required');
                    

                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'Edit Rave';
                    $data['rave'] = $this->rave_model->get_rave_one($rave_id);
                    $data['view'] = 'admin/rave/edit_rave';
                    $this->load->view('admin/layout', $data);
                    
                } else {
                    
                    $description = $this->input->post('description'); 
                    $price = $this->input->post('price'); 
                    
                    if (empty($_FILES['icon_picture']['name'])) {
                        
                        $data = array('price' =>$price,
                                      'description' => $description,
                                      'updated_at' => date('Y-m-d h:m:s') );
                        
                        $this->rave_model->edit_rave($rave_id, $data);
                        $this->session->set_flashdata('msg', 'Rave is Updated Successfully!');             
                        redirect(base_url('admin/rave'), 'refresh');
                        return;
                        
                    }
                    
                
                    if(!is_dir("uploadfiles/")) {
                        mkdir("uploadfiles/");
                    }
                    $upload_path = "uploadfiles/";  

                    $cur_time = time();
                     
                    $dateY = date("Y", $cur_time);
                    $dateM = date("m", $cur_time);
                     
                    if(!is_dir($upload_path."/".$dateY)){
                        mkdir($upload_path."/".$dateY);
                    }
                    if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                        mkdir($upload_path."/".$dateY."/".$dateM);
                    }
                     
                    $upload_path .= $dateY."/".$dateM."/";
                    $upload_url = base_url().$upload_path;

                    // Upload file. 

                    $w_uploadConfig = array(
                        'upload_path' => $upload_path,
                        'upload_url' => $upload_url,
                        'allowed_types' => "*",
                        'overwrite' => TRUE,
                        'max_size' => "100000KB",
                        'max_width' => 4000,
                        'max_height' => 4000,
                        'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
                    );

                    $this->load->library('upload', $w_uploadConfig);

                    if ($this->upload->do_upload('icon_picture')) {

                        // add rave
                        $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                        $data = array('description' => $description,
                                      'prie' => $file_url,
                                      'icon_url' => $file_url,
                                      'updated_at' => date('Y-m-d h:m:s')                                                     
                                      );
                        
                        $this->rave_model->edit_rave($rave_id, $data);
                        $this->session->set_flashdata('msg', 'Rave is Updated Successfully!');             
                        redirect(base_url('admin/rave'), 'refresh');
                        

                    } else {
                        $this->session->set_flashdata('msg', 'Rave update is failed!'); 
                        $data['rave'] = $this->rave_model->get_rave_one($rave_id);            
                        redirect(base_url('admin/rave/edit_rave'), $data);
                    }                    
                }                
            
            } else {
                
                $data['title'] = 'Edit Rave';
                $data['rave'] = $this->rave_model->get_rave_one($rave_id);
                $data['view'] = 'admin/rave/edit_rave';
                $this->load->view('admin/layout', $data);
            }
        }         
                
    }


?>