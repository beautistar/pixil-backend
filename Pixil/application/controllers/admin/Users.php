<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/user_model', 'user_model');
			
		}

        //----------------------------------------------------------------------
        //  Students
		public function index(){
			$data['all_users'] =  $this->user_model->get_all_users();
            $data['title'] = 'Users';
			$data['view'] = 'admin/users/user_list';
			$this->load->view('admin/layout', $data);
		}        
	}


?>