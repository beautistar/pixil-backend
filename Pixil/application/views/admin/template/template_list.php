<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-list"></i> &nbsp; Template List</h3>
        </div>
      </div>
    </div>
  </div>
  
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Mode</th>
          <th>Participant Capacity</th>
          <th>Chargeable</th>
          <th>Created at:</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
          <?php $i = 0;
          foreach($all_template as $row): $i++; ?>
          <tr>
            <td><?= $i; ?></td> 
            <td><?= $row['name']; ?></td>
            <td>
              <span <?= ($row['mode'] == 'public')? 'class="label label-primary"': 'class="label label-warning"' ?>>
                <?= ($row['mode'] == 'public')? 'Public': 'Private' ?>
              </span>
            </td>
            <td><?= $row['participant_capacity']; ?> 
            </td>
            <td><span <?= ($row['is_chargeable'] == 'yes')? 'class="label label-success"': 'class="label label-info"' ?>>
                <?= ($row['is_chargeable'] == 'yes')? 'Chargeable': 'Free' ?>
                </span>
            </td>
            <td><?= ucfirst($row['created_at']); ?></td>

            <td class="text-center">                        
              <a href="<?= base_url('admin/template/edit/'.$row['id']) ?>" class="btn btn-warning btn-flat btn-md">Edit</a>
              <a data-href="<?= base_url('admin/template/del_template/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-md" data-toggle="modal" data-target="#confirm-delete">Delete</a>              
            </td>            
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
<script type="text/javascript">
  $('#confirm-delete').on('show.bs.modal', function(e) {
  $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
</script>
  
<script>
$("#view_raves").addClass('active');
$(document).on("click","#link",function(){
 alert("I am a pop up ! ");
});
</script>

   