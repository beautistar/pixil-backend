<link rel="stylesheet" href="<?= base_url() ?>public/custom_css/upload_button.css">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h3><i class="fa fa-gavel"></i> &nbsp; Edit Template</h3>
        </div>        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/template/edit/'.$template['id']), 'class="form-horizontal"'); ?>
              
              <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label> 
                <div class="col-sm-9">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="<?= $template['name']; ?>" >
                </div>
              </div>              
              
              <div class="form-group">
                <label for="participant_capacity" class="col-sm-3 control-label">Participant Capacity</label>
                <div class="col-sm-9">
                  <input type="number" name="participant_capacity" class="form-control" id="mode" placeholder="Participant Capacity" 
                  value="<?= $template['participant_capacity']; ?>">
                </div>
              </div> 
              
              <div class="form-group">
                <label for="Mode" class="col-sm-3 control-label">Mode</label>

                <div class="col-sm-9">                   
                    <select name="mode" class="form-control" id="mode" placeholder="">
                      <?= $template['mode'] == 'public' ?
                        '<option value="public" selected>Public</option>
                        <option value="private" >Private</option>' 
                        : 
                        '<option value="public" >Public</option>
                        <option value="private" selected>Private</option>'
                      ?>  
                       
                    </select>                  
                </div>
              </div>
              
              <div class="form-group">
                <label for="is_chargeable" class="col-sm-3 control-label">Chargeable</label>

                <div class="col-sm-9">
                    
                    <select name="is_chargeable" class="form-control" id="is_chargeable" placeholder="">
                    
                    <?= $template['is_chargeable'] == 'yes' ?
                      '<option value="yes" selected>Chargeable</option>
                      <option value="no" >Free</option>' 
                      : 
                      '<option value="yes" >Chargeable</option>
                      <option value="no" selected>Free</option>'
                    ?>                      
                    </select>
                  
                </div>
              </div>                                        
  
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 