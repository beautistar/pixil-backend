<?php
  
Class Media_model extends CI_Model {
    
    function add_media($data) {
        
        $this->db->insert('media', $data);
        return $this->db->insert_id();        
    }
    
    function get_post_media($page, $media_type) {        
        
        $start =  ($page - 1) * 10; 
        if ($media_type != 'all') {
            $this->db->where('type', $media_type);            
        }                                   
        $this->db->limit(10, $start);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('media');         
           
        return $query->result_array();       

    }
    
    function get_user_media($user_id) {
        
        $this->db->where('user_id', $user_id);
        return $this->db->get('media')->result_array();
    }
    
    function add_comment($data) {
        
        $this->db->insert('comment', $data);
        return $this->db->insert_id();
    }    
    
    function like_to_friends($data) {
        
        $this->db->insert('like', $data);
        return $this->db->insert_id();
    }
    
    function like_media($data) {
        
        $query = $this->db->where($data)->get('like');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('like', $data);
            return $this->db->insert_id();
            
        } else {
            
            return false;
        }
        
        
    }
    
    function add_hashtag($user_id, $hashtag, $created_at) {
        
        $query = $this->db->where('name', $hashtag)->get('hashtag'); 
        
        if ($query->num_rows() == 0) {
            
            $data = array('user_id' => $user_id,
                          'name' => $hashtag,
                          'post_count' => 1,
                          'created_at' => $created_at);
            $this->db->set($data);
            $this->db->insert('hashtag');
            return $this->db->insert_id();
            
        } else {
            
            $this->db->where('name', $hashtag);
            $this->db->set('post_count', 'post_count+1', FALSE);
            $this->db->update('hashtag');
        }
        
    }
    
    function search_by_username($user_name, $page) {
        
        $start =  ($page - 1) * 10;  
        $this->db->where('user_name', $user_name);      
        $this->db->limit(10, $start);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('media');         
           
        return $query->result();
        
        
    }
    
    function search_by_hashtag($hashtag, $page) {
        
        $start =  ($page - 1) * 10;  
        $this->db->like('hashtag', $hashtag);      
        $this->db->limit(10, $start);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('media');         
           
        return $query->result();       
    }
    
    function search_hashtags($name) {
        
        $this->db->like('name', $name);
        return $this->db->get('hashtag')->result_array();
    }
    
    function post_text($data) {
        
        $this->db->insert('text', $data);
        return $this->db->insert_id();         
    }
    
    function get_post_text($user_id) {
        
        return $this->db->select('B.id, A.name, A.photo_url, B.text')
                    ->from('text as B')
                    ->where('B.user_id', $user_id)
                    ->join('user as A', 'A.id = B.user_id')
                    ->get()
                    ->result_array();            
    }
    
    function get_comment($media_id, $type) {
        
        return $this->db->select('B.id, B.user_id, A.name, A.photo_url, B.comment, B.created_at')
                    ->from('comment as B')
                    ->where('B.media_id', $media_id)
                    ->where('B.type', $type)
                    ->join('user as A', 'A.id = B.user_id')
                    ->get()
                    ->result_array();         
    }
    
    function delete_comment($comment_id) {
        
        $this->db->where('id', $comment_id);
        $this->db->delete('comment');
        return true;
    }
    
    function get_comment_count($media_id, $type) {

                        
        $this->db->where('media_id', $media_id);
        $this->db->where('type', $type);
        return $this->db->get('comment')->num_rows();
        
    }
    
    function get_like_count($media_id, $type) {

                        
        $this->db->where('media_id', $media_id);
        $this->db->where('type', $type);
        return $this->db->get('like')->num_rows();
        
    } 
    
    function get_all_post_text($page) {        
        
        $start =  ($page - 1) * 10; 
        $this->db->limit(10, $start);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('text');         
           
        return $query->result_array();       

    }

}
?>
