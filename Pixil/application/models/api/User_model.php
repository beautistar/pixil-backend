<?php
  
class User_model extends CI_Model {
    
    public function login($data){
        
        $this->db->where('username', $data['username']);
        $this->db->or_where('email', $data['username']);
        $this->db->or_where('phone', $data['username']);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0){
            return false;
        }
        else{
            //Compare the password attempt with the password we have stored.
            $result = $query->row_array();
            $validPassword = password_verify($data['password'], $result['password']);
            if($validPassword){
                return $result = $query->row_array();
            }            
        }
    }
    
    function check_exist($param, $type) {
        
        
        $query = $this->db->where($type, $param);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0) {
            
            return false;
        } else {
            
            return true;
        }
    }
    
    
    public function add_user($data){
        
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    
    function edit_user($id, $data) {
        
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    function update_photo($id, $data) {
        
        $this->db->where('id', $id);
        $this->db->update('user', $data);        
    }  
    
    function get_user($user_id) {
        
        $this->db->where('id', $user_id);
        return $this->db->get('user')->row_array();
    }
    
    function get_user_by_qrcode($qr_code) {
        
        $this->db->where('qr_code', $qr_code);
        $query = $this->db->get('user');
        
        if ($query->num_rows() == 0){
            return false;
        
        } else {

            return $result = $query->row_array();
            
        }            
        
    }    
    
    function add_friend($data) {
        
        $query = $this->db->where($data)->get('friend');
        
        if ($query->num_rows() == 0) {
        
            $this->db->insert('friend', $data);
            return $this->db->insert_id();      
        } else {
            return false;
        }
        
    }
    
    function get_freinds($user_id) {
        
        return $this->db->select('A.id, A.name, A.surname, A.email, A.phone, A.birthday,
                                 A.photo_url, A.qr_code')
                 ->from('friend as B')
                 ->where('A.id !=', $user_id)
                 ->where('B.user_id', $user_id)
                 ->or_where('B.friend_id', $user_id)
                 ->join('user as A', 'A.id = B.user_id')
                 ->get()
                 ->result_array();        
    }
    
    
    function get_users($user_id) {
        
        $this->db->where('id !=',$user_id);
        return $this->db->get('user')->result_array();
    }
    
    function search_user_by_name($user_id, $name) {
        
        $this->db->where('id !=',$user_id);
        $this->db->like('name',$name);
        return $this->db->get('user')->result_array();
    }
    
    
    function check_friend($user_id, $friend_id) {
        
        $sql = 'SELECT * FROM friend WHERE (user_id = '.$user_id.' AND friend_id = '.$friend_id.') 
                                        OR (user_id = '.$friend_id.' AND friend_id = '.$user_id.')';
        $query = $this->db->query($sql);
        
        if ($query->num_rows() == 0) {
            return false;
        } else {
            return true;
        }               
    }
}
?>
