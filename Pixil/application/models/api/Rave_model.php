<?php

class Rave_model extends CI_Model {
    
    function add_rave($data) {
        
        $this->db->insert('rave', $data);
        return $this->db->insert_id();
    }
    
    function get_public_raves() {
        
        $query = $this->db->get_where('rave', array('mode' => 'public', 'is_visible' => 'yes'));
        return $query->result_array();
    }
    
    function add_location($data) {
        
        $query = $this->db->where('name', $data['name'])->get('location');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('location', $data);
            return TRUE;
            
        } else {
            return false;
        }
    }
    
    function set_favorite_location($data) {
        
        $query = $this->db->where($data)->get('favorite_location');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('favorite_location', $data);
            return true;
            
        } else {
            return false;
        }        
    }
    
    function get_favorite_location($user_id) {
        
        
        return $this->db->select('A.*')
                    ->from('favorite_location as B')
                    ->where('B.user_id', $user_id)
                    ->join('location as A', 'A.id = B.location_id')
                    ->get()
                    ->result_array();        
    }
    
    function get_recommended_location($distance, $lat, $lng) {
      
        $sql = "SELECT location.*, tbl2.distance AS distance                        
                    FROM ( SELECT id, distance
                           FROM ( SELECT id,
                                DEGREES(ACOS(
                                    COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                    SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                    ) * 60 * 1.1515 * 1.609344 as distance
                                FROM location
                                ) tb_distance";
                           $sql.=" WHERE distance <= " . $distance;
                    $sql.=") tbl2, location";
                    $sql.=" WHERE tbl2.id = location.id
                    GROUP BY location.id
                    ORDER BY tbl2.distance ASC                     
                    ";
     
        $sql_params = array($lat, $lng, $lat);
        $query = $this->db->query($sql, $sql_params);
        return $query->result_array();
    }
    
    function join_rave($data) {
        
        $query = $this->db->where($data)->get('joined_rave');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('joined_rave', $data);
            return true;
            
        } else {
            return false;
        }
    }
    
    function increase_participants($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('participants', 'participants+1', FALSE);
        $this->db->update('rave');
    }
    
    function get_joined_rave($user_id) {
        
        return $this->db->select('A.*')
                    ->from('joined_rave as B')
                    ->where('B.user_id', $user_id)
                    ->join('rave as A', 'A.id = B.rave_id')
                    ->get()
                    ->result_array();
        
        
    }
    
    function add_wishlist($data) {
        
        $query = $this->db->where($data)->get('wishlist_rave');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('wishlist_rave', $data);
            return true;
            
        } else {
            return false;
        }
    }
    
    function get_wishlist_rave($user_id) {
        
        return $this->db->select('A.*')
                    ->from('wishlist_rave as B')
                    ->where('B.user_id', $user_id)
                    //->where('A.is_visible', 'yes')
                    ->join('rave as A', 'A.id = B.rave_id')
                    ->get()
                    ->result_array();        
    }
    
    function invite_rave($data) {
        
        $query = $this->db->where($data)->get('invite_rave');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('invite_rave', $data);
            return true;
            
        } else {
            return false;
        }
    }
    
    function add_template($data) {
        
        $query = $this->db->get_where('template', array('name' => $data['name']));
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('template', $data);
            return true;
            
        } else {
            return false;
        }
        
        
    }      
    
    function get_template() {
        
        return $this->db->get('template')->result_array();
    }
    
    function add_favorite_template($data) {
        
        $query = $this->db->where($data)->get('favorite_template');
        
        if ($query->num_rows() == 0) {
            
            $this->db->insert('favorite_template', $data);
            return true;
            
        } else {
            return false;
        }        
    }
    
    function get_favorite_template($user_id) {
        
        return $this->db->select('A.*')
                    ->from('favorite_template as B')
                    ->where('B.user_id', $user_id)
                    ->join('template as A', 'A.id = B.template_id')
                    ->get()
                    ->result_array();
        
        
    }
    
    function add_to_igo($data) {
        
        $query = $this->db->where($data)->get('igo');
        
        if ($query->num_rows() == 0) {
            
            $query2 = $this->db->where('user_id', $data['user_id'])->get('igo');
            
            if ($query2->num_rows() < 10) {
                
                $this->db->insert('igo', $data);
                return true;
                
            } else {
                return false;
            }                
            
        } else {
            return false;
        }        
    }
    
    
    function get_igo($user_id) {        
        
        return $this->db->select('A.*')
                    ->from('igo as B')
                    ->where('B.user_id', $user_id)
                    //->where('A.is_visible', 'yes')
                    ->join('rave as A', 'A.id = B.rave_id')
                    ->get()
                    ->result_array();        
    }
    
    function get_ido($user_id) {
        
        return $this->db->select('*')
                    ->from('rave')
                    ->where('owner_id', $user_id)
                    //->where('is_visible', 'yes')
                    ->order_by('id', 'ASC')
                    ->limit(10)
                    ->get()
                    ->result_array();
        
        
    }
    
    function allow_edit($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_edit', 'yes');
        $this->db->update('rave');
    }
    
    function not_allow_edit($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_edit', 'no');
        $this->db->update('rave');
    }
    
    function edit_rave($rave_id, $data) {
        
        $this->db->where('id', $rave_id);
        $this->db->update('rave', $data);
    }
    
    function allow_hide($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_hide', 'yes');
        $this->db->update('rave');
    }
    
    function not_allow_hide($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_hide', 'no');
        $this->db->update('rave');
    }
    
    function allow_invite($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_invite', 'yes');
        $this->db->update('rave');
    }
    
    function not_allow_invite($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_invite', 'no');
        $this->db->update('rave');
    }
    
    function allow_promote($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_promote', 'yes');
        $this->db->update('rave');
    }
    
    function not_allow_promote($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_promote', 'no');
        $this->db->update('rave');
    }
    
    function allow_see_chat($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_see_chat', 'yes');
        $this->db->update('rave');
    }
    
    function not_allow_see_chat($rave_id) {
        
        $this->db->where('id', $rave_id);
        $this->db->set('allow_see_chat', 'no');
        $this->db->update('rave');
    }
    
    function get_statics($rave_id) {
        
        $this->db->select('joined_at, COUNT(user_id) as participants');
        $this->db->where('rave_id', $rave_id);
        $this->db->group_by('joined_at'); 
        $this->db->order_by('joined_at', 'ASC'); 
        return $this->db->get('joined_rave')->result_array();
        
    }

    function get_my_raves($user_id) {

        return $this->db->select('*')
                    ->from('rave')
                    ->where('owner_id', $user_id)                    
                    ->get()
                    ->result_array();


    }
    
     
    
    
    
}
?>
