-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 21, 2019 at 05:17 AM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pixil1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `avatar` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`, `avatar`) VALUES
(3, 'Admin1', 'Sara', 'David', 'admin@admin.com', '123', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, '', '2017-09-29 10:09:44', '2019-10-21 05:33:19', 'avatar/admins/1571635999.jpg'),
(34, 'kang', 'Pixil', 'John', 'admin@admin.com', 'kang', '7454739e907f5595ae61d84b8547f574', 1, 1, 1, '', '2019-08-27 08:03:10', '2019-10-07 12:57:26', 'avatar/admins/avatar.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_setting`
--

CREATE TABLE `tb_app_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `price_intro_eng` text,
  `price_intro_mong` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_app_setting`
--

INSERT INTO `tb_app_setting` (`id`, `price_intro_eng`, `price_intro_mong`) VALUES
(1, '     Buy 3 pixils for ₮50,000. Each additional pixil is ₮15,000.', 'Пиксил багц (3ш): ₮50,000. Нэмэлт пиксил тус бүр: ₮15,000. Хүргэлт үнэгүй');

-- --------------------------------------------------------

--
-- Table structure for table `tb_media`
--

CREATE TABLE `tb_media` (
  `id` int(11) NOT NULL,
  `video` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_media`
--

INSERT INTO `tb_media` (`id`, `video`, `image`) VALUES
(1, 'video/1567045406.mp4', 'image/1569752774.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pixils` int(5) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `zip_code` int(20) NOT NULL,
  `province` varchar(200) NOT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `pixil_price` float NOT NULL,
  `additional_price` float DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `order_date` bigint(20) NOT NULL,
  `print_date` bigint(20) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `emailAddress` varchar(200) NOT NULL,
  `phoneNum` varchar(20) NOT NULL,
  `delivery_date` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `customer_id`, `product_id`, `pixils`, `address`, `district`, `city`, `zip_code`, `province`, `tracking_number`, `pixil_price`, `additional_price`, `status`, `order_date`, `print_date`, `fullName`, `emailAddress`, `phoneNum`, `delivery_date`) VALUES
(1, 21, 1, 4, 'aaa', 'bbb', 'ccc', 12345999, 'ppp', NULL, 50000, 15000, 'Pending', 1569758545, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1570363345),
(52, 17, 1, 6, 'yanbian ', 'yanbian ', 'Yanji ', 123, 'Jilin ', NULL, 50000, 45000, 'Print', 1570682507, 1570732911, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571287307),
(3, 21, 1, 1, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1569802888, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1570407688),
(4, 24, 1, 1, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1569802899, NULL, 'iOS tester', 'beautistar327@gmail.com', '123456', 1570407699),
(5, 24, 1, 2, 'yanji jangbaisan donators ', 'Jilin', 'yanji ', 133000, 'Jilin', NULL, 50000, 0, 'Pending', 1569839882, NULL, 'iOS tester', 'beautistar327@gmail.com', '123456', 1570444682),
(6, 24, 1, 6, 'yanji jangbaisan donators ', 'Jilin', 'yanji ', 133000, 'Jilin', NULL, 50000, 45000, 'Pending', 1569845472, NULL, 'iOS tester', 'beautistar327@gmail.com', '123456', 1570450272),
(7, 24, 1, 5, 'yanji jangbaisan donators ', 'Jilin', 'yanji ', 133000, 'Jilin', NULL, 50000, 30000, 'Pending', 1569848356, NULL, 'iOS tester', 'beautistar327@gmail.com', '123456', 1570453156),
(8, 27, 1, 5, 'shsjsjzkzk', 'sushi ', 'yanji ', 1333, 'ahaha jack', NULL, 50000, 30000, 'Print', 1569942090, 1570539209, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1570546890),
(9, 27, 1, 6, 'shsjsjzkzk', 'sushi ', 'yanji ', 1333, 'ahaha jack', '2019', 50000, 45000, 'Ship', 1569942252, 1570535802, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1570535812),
(10, 16, 3, 3, 'a', 'a', 'a', 0, 'a', NULL, 50000, 0, 'Pending', 1569972600, NULL, 'test iOS', 'beautistar1112@gmail.com', '123456', 1570577400),
(11, 27, 3, 3, 'aaaaaa', 'ddddfff', 'ccccv', 2222, 'ppppp', NULL, 50000, 0, 'Ship', 1569979953, 1569980077, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1570535941),
(12, 27, 1, 10, 'aaaaaa', 'ddddfff', 'ccccv', 2222, 'ppppp', NULL, 50000, 105000, 'Pending', 1569980294, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1570585094),
(13, 27, 3, 3, 'fghh', 'hhhh', 'hhj', 5566, 'ghj', NULL, 50000, 0, 'Ship', 1569982474, 1570536864, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1570536911),
(14, 16, 0, 3, 'thk', 'f.', 'Rhn', 55, 'fn', NULL, 50000, 0, 'Ship', 1570189118, 1570756109, 'test iOS', 'beautistar1112@gmail.com', '123456', 1570756122),
(15, 30, 1, 5, 'Address 2 - Row 1', 'Address 2 - Row 2', 'Ulaanbaatar', 13312, 'Ulaanbaatar', NULL, 50000, 30000, 'Ship', 1570364638, 1570763690, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570764413),
(16, 30, 0, 3, 'Address 1 - Row 1', 'Address 2 - Row 2', 'Ulaanbaatar', 13311, 'Ulaanbaatar', NULL, 50000, 0, 'Ship', 1570365260, 1570979741, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570979749),
(17, 31, 2, 4, 'Address 3 - Row 1', 'Address 3 - Row 2', 'Ulaanbaatar', 13313, 'UB', NULL, 50000, 15000, 'Pending', 1570366686, NULL, 'Firstname_3 Lastname_3', 'email_3@gmail.com', '99995019', 1570971486),
(18, 30, 2, 3, 'Address 4 - Row 1', 'Address 4 - Row 2', 'Ulaanbaatar', 13314, 'UB', NULL, 50000, 0, 'Print', 1570367690, 1570979800, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570972490),
(19, 30, 2, 3, 'Address 3 - Row 1', 'Address 3 - Row 2', 'Ulaanbaatar', 13313, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1570367790, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570972590),
(20, 30, 0, 3, 'Address 1 - Row 1', 'Address 2 - Row 2', 'Ulaanbaatar', 13311, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1570367889, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570972689),
(21, 30, 0, 3, 'Address 1 - Row 1', 'Address 2 - Row 2', 'Ulaanbaatar', 13311, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1570367919, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570972719),
(22, 30, 0, 3, 'Entrying address again', 'address row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570368128, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570972928),
(23, 31, 3, 3, 'Address 2 - Row 1', 'Address 2 - Row 2', 'Ulaanbaatar', 13312, 'UB', NULL, 50000, 0, 'Pending', 1570368271, NULL, 'Firstname_3 Lastname_3', 'email_3@gmail.com', '99995019', 1570973071),
(24, 30, 0, 3, 'dhdjd', 'jdjdm', 'Jdmxj', 131313, 'dhdjsj', NULL, 50000, 0, 'Pending', 1570368521, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570973321),
(25, 30, 0, 3, 'dhdjd', 'jdjdm', 'Jdmxj', 131313, 'dhdjsj', NULL, 50000, 0, 'Pending', 1570368544, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1570973344),
(26, 21, 1, 4, 'qqq', 'qqq', 'aaa', 123456, 'bbb', NULL, 50000, 15000, 'Pending', 1570400226, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571005026),
(27, 31, 1, 3, 'Address 3 - Row 1', 'Address 3 - Row 2', 'Ulaanbaatar', 13313, 'UB', 'TRC3412', 50000, 0, 'Pending', 1570412096, 1570559137, 'Firstname_3 Lastname_3', 'email_3@gmail.com', '99995019', 1570440327),
(28, 31, 3, 3, 'Address 4 - Row 1', 'Address 4 - Row 2', 'Ulaanbaatar', 13314, 'UB', NULL, 50000, 0, 'Ship', 1570418390, 1570418572, 'Firstname_3 Lastname_3', 'email_3@gmail.com', '99995019', 2019),
(29, 31, 3, 3, 'Address 4 - Row 1', 'Address 4 - Row 2', 'Ulaanbaatar', 13314, 'UB', NULL, 50000, 0, 'Pending', 1570418398, NULL, 'Firstname_3 Lastname_3', 'email_3@gmail.com', '99995019', 1571023198),
(30, 30, 2, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13311, 'UB', NULL, 50000, 0, 'Pending', 1570421864, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571026664),
(31, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13311, 'UB', NULL, 50000, 0, 'Pending', 1570422140, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571026940),
(32, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13311, 'UB', NULL, 50000, 0, 'Pending', 1570422351, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571027151),
(33, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13311, 'UB', NULL, 50000, 0, 'Pending', 1570422869, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571027669),
(34, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425564, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030364),
(35, 30, 1, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425585, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030385),
(36, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425781, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030581),
(37, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425803, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030603),
(38, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425829, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030629),
(39, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425849, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030649),
(40, 30, 2, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425871, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030671),
(41, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425966, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030766),
(42, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570425997, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030797),
(43, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570426030, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030830),
(44, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570426071, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571030871),
(45, 30, 0, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570426245, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571031045),
(46, 30, 1, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570426376, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571031176),
(47, 30, 2, 3, 'Address 1 - Row 1', 'Address 1 - Row 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1570426590, NULL, 'Firstname_test2 Lastname_test2', 'email_test2@gmail.com', '99415039', 1571031390),
(48, 17, 0, 3, 'yanbian', 'yanbian', 'Yanji', 123, 'jilin', NULL, 50000, 0, 'Pending', 1570585824, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571190624),
(49, 17, 1, 3, 'yanbian', 'yanbian', 'Yanji', 123, 'jilin', NULL, 50000, 0, 'Pending', 1570587398, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571192198),
(50, 17, 0, 4, 'yanbian', 'yanbian', 'Yanji', 123, 'jilin', NULL, 50000, 0, 'Pending', 1570587489, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571192289),
(51, 17, 0, 3, 'yanbian', 'yanbian', 'Yanji', 123, 'jilin', NULL, 50000, 0, 'Pending', 1570592415, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571197215),
(53, 17, 1, 4, 'yanbian ', 'yanbian ', 'Yanji ', 123, 'Jilin ', NULL, 50000, 15000, 'Pending', 1570684170, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1571288970),
(57, 27, 1, 4, 'g', 'h', 'g', 5, 'g', NULL, 50000, 15000, 'Pending', 1571231327, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1571836127),
(56, 27, 3, 3, 'g', 'h', 'g', 5, 'g', NULL, 50000, 0, 'Pending', 1571226913, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1571831713),
(58, 27, 2, 5, 'g', 'h', 'g', 5, 'g', NULL, 50000, 30000, 'Pending', 1571234686, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1571839486),
(59, 27, 1, 3, 'g', 'h', 'g', 5, 'g', NULL, 50000, 0, 'Pending', 1571235917, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1571840717),
(60, 27, 3, 3, 'Changbaishan East load 123', 'Jilin', 'Yanji', 133000, 'Yanbian', NULL, 50000, 0, 'Pending', 1571282392, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1571887192),
(61, 21, 1, 0, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1571282453, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571887253),
(62, 21, 1, 1, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1571282969, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571887769),
(63, 21, 1, 2, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1571283091, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571887891),
(64, 21, 1, 4, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 15000, 'Pending', 1571362186, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571966986),
(65, 21, 1, 8, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 75000, 'Pending', 1571362232, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1571967032),
(66, 34, 0, 3, 'Address line 1', 'Address line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571368919, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1571973719),
(67, 34, 0, 2, 'Address line 1', 'Address line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571369817, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1571974617),
(68, 34, 1, 1, 'Address line 1', 'Address line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571369897, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1571974697),
(69, 34, 2, 1, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1571370273, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1571975073),
(70, 34, 2, 1, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'UB', NULL, 50000, 0, 'Pending', 1571376009, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1571980809),
(71, 33, 3, 1, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571376273, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1571981073),
(72, 33, 1, 1, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571376586, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1571981386),
(73, 33, 2, 1, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571376862, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1571981662),
(74, 33, 3, 3, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571390588, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1571995388),
(75, 17, 1, 3, 'yanbian ', 'yanbian ', 'Yanji ', 123, 'Jilin ', NULL, 50000, 0, 'Pending', 1571396176, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1572000976),
(76, 17, 1, 3, 'yanbian ', 'yanbian ', 'Yanji ', 123, 'Jilin ', NULL, 50000, 0, 'Pending', 1571396980, NULL, 'James John ', 'alertingjames@gmail.com', '1234567890', 1572001780),
(77, 27, 1, 3, 'Apple home', 'distraction', 'New York', 12324, 'San Francisco', NULL, 50000, 0, 'Pending', 1571537045, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572141845),
(78, 27, 1, 3, 'Apple home', 'distraction', 'New York', 12324, 'San Francisco', NULL, 50000, 0, 'Pending', 1571537197, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572141997),
(79, 27, 1, 3, 'Apple home', 'distraction', 'New York', 12324, 'San Francisco', NULL, 50000, 0, 'Pending', 1571554843, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572159643),
(80, 27, 1, 3, 'Apple home', 'distraction', 'New York', 12324, 'San Francisco', NULL, 50000, 0, 'Pending', 1571555589, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572160389),
(81, 27, 1, 3, 'Changbaishan East load 123', 'Jilin', 'Yanji', 133000, 'Yanbian', NULL, 50000, 0, 'Pending', 1571561784, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572166584),
(82, 27, 1, 3, 'Apple home', 'distraction', 'New York', 12324, 'San Francisco', NULL, 50000, 0, 'Pending', 1571585004, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572189804),
(83, 21, 1, 3, 'nanbei', 'beilu', 'dandong', 12345, 'jilin', NULL, 50000, 0, 'Pending', 1571597946, NULL, 'Test Tester1', 'here0919@gmail.com', '1234567890', 1572202746),
(84, 34, 2, 2, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571622200, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1572227000),
(85, 34, 0, 1, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571623079, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1572227879),
(86, 34, 1, 1, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571623647, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1572228447),
(87, 34, 2, 1, 'Address 1', 'Address 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571623764, NULL, 'Michael Jordan', 'jordan@gmail.com', '99415039', 1572228564),
(88, 33, 1, 3, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571629718, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1572234518),
(89, 33, 2, 3, 'Address Line 1', 'Address Line 2', 'Ulaanbaatar', 13331, 'Ulaanbaatar', NULL, 50000, 0, 'Pending', 1571630090, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1572234890),
(90, 33, 1, 3, 'Peace Avenue 5, #9', 'Chingeltei district, 1 khoroo', 'Ulaanbaatar', 15710, 'Ulaanbaatar', NULL, 50000, 0, 'Print', 1571634602, 1571664441, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1572239402),
(91, 27, 2, 4, 'hhjj', 'ghjj', 'hj', 556, 'hjjkk', NULL, 50000, 15000, 'Pending', 1571649372, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572254172),
(92, 27, 3, 5, 'hhjj', 'ghjj', 'hj', 556, 'hjjkk', NULL, 50000, 30000, 'Pending', 1571649670, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572254470),
(93, 27, 1, 4, 'hhjj', 'ghjj', 'hj', 556, 'hjjkk', NULL, 50000, 15000, 'Pending', 1571649820, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572254620),
(94, 27, 1, 4, 'hhjj', 'ghjj', 'hj', 556, 'hjjkk', NULL, 50000, 15000, 'Pending', 1571649822, NULL, 'iPhone 5s', 'alexmad327@gmail.com', '1', 1572254622);

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_setting`
--

CREATE TABLE `tb_order_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `minimum_order_quantity` int(10) DEFAULT NULL,
  `minimum_order_pricing` int(10) DEFAULT NULL,
  `shipping_cost` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_order_setting`
--

INSERT INTO `tb_order_setting` (`id`, `minimum_order_quantity`, `minimum_order_pricing`, `shipping_cost`) VALUES
(1, 19, 123, 4324);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pixil`
--

CREATE TABLE `tb_pixil` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pixil`
--

INSERT INTO `tb_pixil` (`id`, `order_id`, `url`) VALUES
(1, 1, 'avatar/pixils015697585454.png'),
(2, 1, 'avatar/pixils115697585454.png'),
(3, 1, 'avatar/pixils215697585454.png'),
(4, 1, 'avatar/pixils315697585454.png'),
(5, 2, 'avatar/pixils015697595679.png'),
(6, 2, 'avatar/pixils115697595679.png'),
(7, 2, 'avatar/pixils215697595679.png'),
(8, 2, 'avatar/pixils315697595680.png'),
(9, 2, 'avatar/pixils415697595680.png'),
(10, 2, 'avatar/pixils515697595680.png'),
(11, 3, 'avatar/pixils015698028887.png'),
(12, 4, 'avatar/pixils015698028996.png'),
(13, 5, 'avatar/pixils015698398825.png'),
(14, 5, 'avatar/pixils115698398825.png'),
(15, 6, 'avatar/pixils015698454725.png'),
(16, 6, 'avatar/pixils115698454725.png'),
(17, 6, 'avatar/pixils215698454725.png'),
(18, 6, 'avatar/pixils315698454725.png'),
(19, 6, 'avatar/pixils415698454725.png'),
(20, 6, 'avatar/pixils515698454725.png'),
(21, 7, 'avatar/pixils015698483568.png'),
(22, 7, 'avatar/pixils115698483568.png'),
(23, 7, 'avatar/pixils215698483568.png'),
(24, 7, 'avatar/pixils315698483568.png'),
(25, 7, 'avatar/pixils415698483568.png'),
(26, 8, 'avatar/pixils015699420905.png'),
(27, 8, 'avatar/pixils115699420905.png'),
(28, 8, 'avatar/pixils215699420905.png'),
(29, 8, 'avatar/pixils315699420905.png'),
(30, 8, 'avatar/pixils415699420905.png'),
(31, 9, 'avatar/pixils015699422525.png'),
(32, 9, 'avatar/pixils115699422525.png'),
(33, 9, 'avatar/pixils215699422525.png'),
(34, 9, 'avatar/pixils315699422525.png'),
(35, 9, 'avatar/pixils415699422525.png'),
(36, 9, 'avatar/pixils515699422525.png'),
(37, 10, 'avatar/pixils015699726008.png'),
(38, 10, 'avatar/pixils115699726008.png'),
(39, 10, 'avatar/pixils215699726008.png'),
(40, 11, 'avatar/pixils015699799535.png'),
(41, 11, 'avatar/pixils115699799535.png'),
(42, 11, 'avatar/pixils215699799535.png'),
(43, 12, 'avatar/pixils015699802943.png'),
(44, 12, 'avatar/pixils115699802971.png'),
(45, 12, 'avatar/pixils215699802971.png'),
(46, 12, 'avatar/pixils315699802977.png'),
(47, 12, 'avatar/pixils415699802977.png'),
(48, 12, 'avatar/pixils515699802977.png'),
(49, 12, 'avatar/pixils615699802977.png'),
(50, 12, 'avatar/pixils715699802977.png'),
(51, 12, 'avatar/pixils815699802977.png'),
(52, 12, 'avatar/pixils915699802977.png'),
(53, 13, 'avatar/pixils015699824746.png'),
(54, 13, 'avatar/pixils115699824746.png'),
(55, 13, 'avatar/pixils215699824746.png'),
(56, 14, 'avatar/pixils015701891184.png'),
(57, 14, 'avatar/pixils115701891184.png'),
(58, 14, 'avatar/pixils215701891184.png'),
(59, 15, 'avatar/pixils015703646383.png'),
(60, 15, 'avatar/pixils115703646411.png'),
(61, 15, 'avatar/pixils215703646411.png'),
(62, 15, 'avatar/pixils315703646411.png'),
(63, 15, 'avatar/pixils415703646411.png'),
(64, 16, 'avatar/pixils015703652608.png'),
(65, 16, 'avatar/pixils115703652615.png'),
(66, 16, 'avatar/pixils215703652617.png'),
(67, 17, 'avatar/pixils015703666862.png'),
(68, 17, 'avatar/pixils115703666885.png'),
(69, 17, 'avatar/pixils215703666885.png'),
(70, 17, 'avatar/pixils315703666885.png'),
(71, 18, 'avatar/pixils015703676901.png'),
(72, 18, 'avatar/pixils115703676927.png'),
(73, 18, 'avatar/pixils215703676932.png'),
(74, 19, 'avatar/pixils015703677905.png'),
(75, 19, 'avatar/pixils115703677936.png'),
(76, 19, 'avatar/pixils215703677957.png'),
(77, 20, 'avatar/pixils015703678895.png'),
(78, 20, 'avatar/pixils115703678925.png'),
(79, 20, 'avatar/pixils215703678928.png'),
(80, 21, 'avatar/pixils015703679195.png'),
(81, 21, 'avatar/pixils115703679220.png'),
(82, 21, 'avatar/pixils215703679223.png'),
(83, 22, 'avatar/pixils015703681288.png'),
(84, 22, 'avatar/pixils115703681314.png'),
(85, 22, 'avatar/pixils215703681317.png'),
(86, 23, 'avatar/pixils015703682717.png'),
(87, 23, 'avatar/pixils115703682717.png'),
(88, 23, 'avatar/pixils215703682744.png'),
(89, 24, 'avatar/pixils015703685215.png'),
(90, 24, 'avatar/pixils115703685250.png'),
(91, 24, 'avatar/pixils215703685253.png'),
(92, 25, 'avatar/pixils015703685444.png'),
(93, 25, 'avatar/pixils115703685467.png'),
(94, 25, 'avatar/pixils215703685473.png'),
(95, 26, 'avatar/pixils015704002266.png'),
(96, 26, 'avatar/pixils115704002266.png'),
(97, 26, 'avatar/pixils215704002273.png'),
(98, 26, 'avatar/pixils315704002274.png'),
(99, 27, 'avatar/pixils015704120961.png'),
(100, 27, 'avatar/pixils115704120978.png'),
(101, 27, 'avatar/pixils215704120978.png'),
(102, 28, 'avatar/pixils015704183906.png'),
(103, 28, 'avatar/pixils115704183906.png'),
(104, 28, 'avatar/pixils215704183906.png'),
(105, 29, 'avatar/pixils015704183988.png'),
(106, 29, 'avatar/pixils115704183988.png'),
(107, 29, 'avatar/pixils215704183988.png'),
(108, 30, 'avatar/pixils015704218641.png'),
(109, 30, 'avatar/pixils115704218641.png'),
(110, 30, 'avatar/pixils215704218667.png'),
(111, 31, 'avatar/pixils015704221405.png'),
(112, 31, 'avatar/pixils115704221448.png'),
(113, 31, 'avatar/pixils215704221448.png'),
(114, 32, 'avatar/pixils015704223510.png'),
(115, 32, 'avatar/pixils115704223510.png'),
(116, 32, 'avatar/pixils215704223510.png'),
(117, 33, 'avatar/pixils015704228690.png'),
(118, 33, 'avatar/pixils115704228709.png'),
(119, 33, 'avatar/pixils215704228714.png'),
(120, 34, 'avatar/pixils015704255643.png'),
(121, 34, 'avatar/pixils115704255644.png'),
(122, 34, 'avatar/pixils215704255644.png'),
(123, 35, 'avatar/pixils015704255854.png'),
(124, 35, 'avatar/pixils115704255854.png'),
(125, 35, 'avatar/pixils215704255854.png'),
(126, 36, 'avatar/pixils015704257816.png'),
(127, 36, 'avatar/pixils115704257816.png'),
(128, 36, 'avatar/pixils215704257816.png'),
(129, 37, 'avatar/pixils015704258038.png'),
(130, 37, 'avatar/pixils115704258038.png'),
(131, 37, 'avatar/pixils215704258038.png'),
(132, 38, 'avatar/pixils015704258293.png'),
(133, 38, 'avatar/pixils115704258293.png'),
(134, 38, 'avatar/pixils215704258293.png'),
(135, 39, 'avatar/pixils015704258496.png'),
(136, 39, 'avatar/pixils115704258496.png'),
(137, 39, 'avatar/pixils215704258496.png'),
(138, 40, 'avatar/pixils015704258713.png'),
(139, 40, 'avatar/pixils115704258713.png'),
(140, 40, 'avatar/pixils215704258713.png'),
(141, 41, 'avatar/pixils015704259660.png'),
(142, 41, 'avatar/pixils115704259676.png'),
(143, 41, 'avatar/pixils215704259676.png'),
(144, 42, 'avatar/pixils015704259976.png'),
(145, 42, 'avatar/pixils115704259976.png'),
(146, 42, 'avatar/pixils215704259976.png'),
(147, 43, 'avatar/pixils015704260301.png'),
(148, 43, 'avatar/pixils115704260301.png'),
(149, 43, 'avatar/pixils215704260301.png'),
(150, 44, 'avatar/pixils015704260713.png'),
(151, 44, 'avatar/pixils115704260713.png'),
(152, 44, 'avatar/pixils215704260713.png'),
(153, 45, 'avatar/pixils015704262457.png'),
(154, 45, 'avatar/pixils115704262478.png'),
(155, 45, 'avatar/pixils215704262478.png'),
(156, 46, 'avatar/pixils015704263763.png'),
(157, 46, 'avatar/pixils115704263785.png'),
(158, 46, 'avatar/pixils215704263786.png'),
(159, 47, 'avatar/pixils015704265909.png'),
(160, 47, 'avatar/pixils115704265927.png'),
(161, 47, 'avatar/pixils215704265931.png'),
(162, 48, 'avatar/pixils015705858246.png'),
(163, 48, 'avatar/pixils115705858316.png'),
(164, 48, 'avatar/pixils215705858316.png'),
(165, 49, 'avatar/pixils015705873985.png'),
(166, 49, 'avatar/pixils115705873985.png'),
(167, 49, 'avatar/pixils215705873985.png'),
(168, 50, 'avatar/pixils015705874899.png'),
(169, 50, 'avatar/pixils115705874899.png'),
(170, 50, 'avatar/pixils215705874899.png'),
(171, 50, 'avatar/pixils315705874899.png'),
(172, 51, 'avatar/pixils015705924155.png'),
(173, 51, 'avatar/pixils115705924155.png'),
(174, 51, 'avatar/pixils215705924206.png'),
(175, 52, 'avatar/pixils015706825078.png'),
(176, 52, 'avatar/pixils115706825078.png'),
(177, 52, 'avatar/pixils215706825078.png'),
(178, 52, 'avatar/pixils315706825078.png'),
(179, 52, 'avatar/pixils415706825078.png'),
(180, 52, 'avatar/pixils515706825088.png'),
(181, 53, 'avatar/pixils015706841709.png'),
(182, 53, 'avatar/pixils115706841709.png'),
(183, 53, 'avatar/pixils215706841709.png'),
(184, 53, 'avatar/pixils315706841709.png'),
(185, 54, 'avatar/pixils015711546988.png'),
(186, 54, 'avatar/pixils115711547008.png'),
(187, 54, 'avatar/pixils215711547010.png'),
(188, 55, 'avatar/pixils015711550152.png'),
(189, 55, 'avatar/pixils115711550153.png'),
(190, 55, 'avatar/pixils215711550166.png'),
(191, 56, 'avatar/pixils015712269134.png'),
(192, 56, 'avatar/pixils115712269135.png'),
(193, 56, 'avatar/pixils215712269135.png'),
(194, 57, 'avatar/pixils015712313280.png'),
(195, 57, 'avatar/pixils115712313294.png'),
(196, 57, 'avatar/pixils215712313311.png'),
(197, 57, 'avatar/pixils315712313311.png'),
(198, 58, 'avatar/pixils015712346867.png'),
(199, 58, 'avatar/pixils115712346921.png'),
(200, 58, 'avatar/pixils215712346921.png'),
(201, 58, 'avatar/pixils315712346921.png'),
(202, 58, 'avatar/pixils415712346921.png'),
(203, 59, 'avatar/pixils015712359173.png'),
(204, 59, 'avatar/pixils115712359203.png'),
(205, 59, 'avatar/pixils215712359267.png'),
(206, 60, 'avatar/pixils015712823926.png'),
(207, 60, 'avatar/pixils115712824050.png'),
(208, 60, 'avatar/pixils215712824164.png'),
(209, 62, 'avatar/pixils015712829695.png'),
(210, 63, 'avatar/pixils015712830918.png'),
(211, 63, 'avatar/pixils115712830918.png'),
(212, 64, 'avatar/pixils015713621860.png'),
(213, 64, 'avatar/pixils115713621860.png'),
(214, 64, 'avatar/pixils215713621860.png'),
(215, 64, 'avatar/pixils315713621860.png'),
(216, 65, 'avatar/pixils015713622326.png'),
(217, 65, 'avatar/pixils115713622326.png'),
(218, 65, 'avatar/pixils215713622326.png'),
(219, 65, 'avatar/pixils315713622332.png'),
(220, 65, 'avatar/pixils415713622332.png'),
(221, 65, 'avatar/pixils515713622332.png'),
(222, 65, 'avatar/pixils615713622332.png'),
(223, 65, 'avatar/pixils715713622332.png'),
(224, 66, 'avatar/pixils015713689195.png'),
(225, 66, 'avatar/pixils115713689256.png'),
(226, 66, 'avatar/pixils215713689257.png'),
(227, 67, 'avatar/pixils015713698176.png'),
(228, 67, 'avatar/pixils115713698231.png'),
(229, 68, 'avatar/pixils015713698970.png'),
(230, 69, 'avatar/pixils015713702739.png'),
(231, 70, 'avatar/pixils015713760091.png'),
(232, 71, 'avatar/pixils015713762736.png'),
(233, 72, 'avatar/pixils015713765860.png'),
(234, 73, 'avatar/pixils015713768628.png'),
(235, 74, 'avatar/pixils015713905887.png'),
(236, 74, 'avatar/pixils115713905964.png'),
(237, 74, 'avatar/pixils215713905965.png'),
(238, 75, 'avatar/pixils015713961764.png'),
(239, 75, 'avatar/pixils115713961764.png'),
(240, 75, 'avatar/pixils215713961764.png'),
(241, 76, 'avatar/pixils015713969806.png'),
(242, 76, 'avatar/pixils115713969806.png'),
(243, 76, 'avatar/pixils215713969806.png'),
(244, 77, 'avatar/pixils015715370459.png'),
(245, 77, 'avatar/pixils115715370658.png'),
(246, 77, 'avatar/pixils215715370687.png'),
(247, 78, 'avatar/pixils015715371978.png'),
(248, 78, 'avatar/pixils115715372200.png'),
(249, 78, 'avatar/pixils215715372265.png'),
(250, 79, 'avatar/pixils015715548434.png'),
(251, 79, 'avatar/pixils115715548652.png'),
(252, 79, 'avatar/pixils215715548672.png'),
(253, 80, 'avatar/pixils015715555890.png'),
(254, 80, 'avatar/pixils115715556085.png'),
(255, 80, 'avatar/pixils215715556128.png'),
(256, 81, 'avatar/pixils015715617849.png'),
(257, 81, 'avatar/pixils115715617999.png'),
(258, 81, 'avatar/pixils215715618070.png'),
(259, 82, 'avatar/pixils015715850049.png'),
(260, 82, 'avatar/pixils115715850266.png'),
(261, 82, 'avatar/pixils215715850337.png'),
(262, 83, 'avatar/pixils015715979462.png'),
(263, 83, 'avatar/pixils115715979607.png'),
(264, 83, 'avatar/pixils215715979730.png'),
(265, 84, 'avatar/pixils015716222006.png'),
(266, 84, 'avatar/pixils115716222049.png'),
(267, 85, 'avatar/pixils015716230796.png'),
(268, 86, 'avatar/pixils015716236471.png'),
(269, 87, 'avatar/pixils015716237648.png'),
(270, 88, 'avatar/pixils015716297188.png'),
(271, 88, 'avatar/pixils115716297294.png'),
(272, 88, 'avatar/pixils215716297389.png'),
(273, 89, 'avatar/pixils015716300902.png'),
(274, 89, 'avatar/pixils115716300997.png'),
(275, 89, 'avatar/pixils215716301014.png'),
(276, 90, 'avatar/pixils015716346029.png'),
(277, 90, 'avatar/pixils115716346030.png'),
(278, 90, 'avatar/pixils215716346030.png'),
(279, 91, 'avatar/pixils015716493723.png'),
(280, 91, 'avatar/pixils115716493723.png'),
(281, 91, 'avatar/pixils215716493723.png'),
(282, 91, 'avatar/pixils315716493723.png'),
(283, 92, 'avatar/pixils015716496702.png'),
(284, 92, 'avatar/pixils115716496702.png'),
(285, 92, 'avatar/pixils215716496702.png'),
(286, 92, 'avatar/pixils315716496702.png'),
(287, 92, 'avatar/pixils415716496702.png'),
(288, 93, 'avatar/pixils015716498203.png'),
(289, 93, 'avatar/pixils115716498203.png'),
(290, 93, 'avatar/pixils215716498209.png'),
(291, 93, 'avatar/pixils315716498209.png'),
(292, 94, 'avatar/pixils015716498224.png'),
(293, 94, 'avatar/pixils115716498238.png'),
(294, 94, 'avatar/pixils215716498238.png'),
(295, 94, 'avatar/pixils315716498238.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL,
  `url_front` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_mong` varchar(200) NOT NULL,
  `name_eng` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `photo_size_width` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `photo_size_height` int(10) NOT NULL,
  `frame_size_width` int(10) NOT NULL,
  `frame_size_height` int(10) NOT NULL,
  `url_right` varchar(255) NOT NULL,
  `url_bottom` varchar(255) NOT NULL,
  `product_size_width` int(10) NOT NULL,
  `product_size_height` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`id`, `url_front`, `name_mong`, `name_eng`, `photo_size_width`, `price`, `photo_size_height`, `frame_size_width`, `frame_size_height`, `url_right`, `url_bottom`, `product_size_width`, `product_size_height`) VALUES
(1, 'avatar/products/1569757797.png', 'Modern', 'Modern', 703, 15000, 703, 703, 703, 'avatar/products/15697577971.png', 'avatar/products/15697577972.png', 20, 20),
(2, 'avatar/products/1569757866.png', 'White', 'White', 703, 15000, 703, 654, 654, 'avatar/products/15697578661.png', 'avatar/products/15697578662.png', 20, 20),
(3, 'avatar/products/1569758004.png', 'Classic', 'Classic', 703, 15000, 703, 503, 503, 'avatar/products/15697580041.png', 'avatar/products/15697580042.png', 20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tb_promo`
--

CREATE TABLE `tb_promo` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `off` int(10) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `allowed` int(10) DEFAULT NULL,
  `redeemed` int(10) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_promo`
--

INSERT INTO `tb_promo` (`id`, `code`, `off`, `start_date`, `end_date`, `allowed`, `redeemed`, `date`) VALUES
(1, 'FALLNIGHT2019', 30, '2019-10-06 04:25:22', '2019-11-01 04:25:22', 12, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_shipaddress`
--

CREATE TABLE `tb_shipaddress` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `phoneNum` varchar(200) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `u_id` int(11) NOT NULL,
  `u_firstname` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `u_lastname` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `u_email` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `u_phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `u_password` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_date` date NOT NULL,
  `reg_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`u_id`, `u_firstname`, `u_lastname`, `u_email`, `u_phone`, `u_password`, `last_date`, `reg_date`) VALUES
(21, 'Test', 'Tester1', 'here0919@gmail.com', '1234567890', 'e10adc3949ba59abbe56e057f20f883e', '2019-10-18', '0000-00-00'),
(17, 'James', 'John ', 'alertingjames@gmail.com', '1234567890', '098f6bcd4621d373cade4e832627b4f6', '2019-10-21', '0000-00-00'),
(16, 'test', 'iOS', 'beautistar1112@gmail.com', '123456', '827ccb0eea8a706c4c34a16891f84e7b', '2019-10-04', '0000-00-00'),
(26, 'jack', 'ok', 'lonelylovelyman@hotmail.com', '12345678', 'fcea920f7412b5da7be0cf42b8c93759', '2019-10-04', '2019-09-29'),
(25, 'Test', 'Tester2', 'bright.sun667@hotmail.com', '1234567890', 'e10adc3949ba59abbe56e057f20f883e', '2019-09-29', '0000-00-00'),
(24, 'iOS', 'tester', 'beautistar327@gmail.com', '123456', 'e10adc3949ba59abbe56e057f20f883e', '2019-10-01', '0000-00-00'),
(27, 'iPhone', '5s', 'alexmad327@gmail.com', '1', 'e10adc3949ba59abbe56e057f20f883e', '2019-10-21', '2019-10-01'),
(28, 'Jiang', 'Cheng', 'jiang@admin.com', '123123', '7454739e907f5595ae61d84b8547f574', '2019-10-02', '2019-10-02'),
(29, 'Alianna', 'Jane', 'aliannajane18715@gmail.com', '1234567890', '098f6bcd4621d373cade4e832627b4f6', '2019-10-04', '2019-10-04'),
(34, 'Michael', 'Jordan', 'jordan@gmail.com', '99415039', '5f4dcc3b5aa765d61d8327deb882cf99', '2019-10-21', '2019-10-18'),
(33, 'Ankhbayar', 'Batbaatar', 'ankhbayar@gmail.com', '99995019', '5f4dcc3b5aa765d61d8327deb882cf99', '2019-10-21', '2019-10-17'),
(32, 'Johnny ', 'Lulz', 'pulz99@hotmail.com', '308526556', '81dc9bdb52d04dc20036dbd8313ed055', '2019-10-14', '2019-10-14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_app_setting`
--
ALTER TABLE `tb_app_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_media`
--
ALTER TABLE `tb_media`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_order_setting`
--
ALTER TABLE `tb_order_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_pixil`
--
ALTER TABLE `tb_pixil`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_promo`
--
ALTER TABLE `tb_promo`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_shipaddress`
--
ALTER TABLE `tb_shipaddress`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`u_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tb_app_setting`
--
ALTER TABLE `tb_app_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_media`
--
ALTER TABLE `tb_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `tb_order_setting`
--
ALTER TABLE `tb_order_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_pixil`
--
ALTER TABLE `tb_pixil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_promo`
--
ALTER TABLE `tb_promo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_shipaddress`
--
ALTER TABLE `tb_shipaddress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
