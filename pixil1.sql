-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 06, 2020 at 05:25 AM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pixil1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `avatar` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`, `avatar`) VALUES
(3, 'Bolor', 'Boloroo', '.K', 'admin@pixil.mn', '123', 'f5b8e91e49e5d43b6214cc0352dbd3e6', 1, 1, 1, '', '2017-09-29 10:09:44', '2020-01-20 02:00:22', 'avatar/admins/1571635999.jpg'),
(44, 'Ankha', 'Ankhbayar', '.B', 'ankhbayar@gmail.com', '123', 'cefaf7bba6d92f0df519300b99daf837', 1, 1, 0, '', '2020-01-16 14:28:11', '2020-01-20 02:00:37', 'avatar/admins/1579185223.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_setting`
--

CREATE TABLE `tb_app_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `price_intro_eng` text,
  `price_intro_mong` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_app_setting`
--

INSERT INTO `tb_app_setting` (`id`, `price_intro_eng`, `price_intro_mong`) VALUES
(1, '     Buy 3 pixils for ₮50,000. Each additional pixil is ₮15,000.', 'Пиксил багц (3ш): ₮50,000. Нэмэлт пиксил тус бүр: ₮15,000. Хүргэлт үнэгүй');

-- --------------------------------------------------------

--
-- Table structure for table `tb_media`
--

CREATE TABLE `tb_media` (
  `id` int(11) NOT NULL,
  `video` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_media`
--

INSERT INTO `tb_media` (`id`, `video`, `image`) VALUES
(1, 'video/1567045406.mp4', 'image/1569752774.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pixils` int(5) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `zip_code` int(20) NOT NULL,
  `province` varchar(200) NOT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `pixil_price` float NOT NULL,
  `additional_price` float DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `order_date` bigint(20) NOT NULL,
  `print_date` bigint(20) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `emailAddress` varchar(200) NOT NULL,
  `phoneNum` varchar(20) NOT NULL,
  `delivery_date` bigint(20) NOT NULL,
  `payment_status` varchar(120) NOT NULL DEFAULT 'NOT PAID'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `customer_id`, `product_id`, `pixils`, `address`, `district`, `city`, `zip_code`, `province`, `tracking_number`, `pixil_price`, `additional_price`, `status`, `order_date`, `print_date`, `fullName`, `emailAddress`, `phoneNum`, `delivery_date`, `payment_status`) VALUES
(1262, 39, 1, 3, 'Khans', '15', 'Ub', 0, 'mongol', NULL, 50000, 0, 'Pending', 1578993168, NULL, 'Galtbolor Khishigbayar', 'galtbolor@yahoo.com', '99178190', 1579597968, 'PAID'),
(1290, 72, 1, 7, '229-118', 'sukhbaatar 9 xoroo dolgoonnuur', 'Ulaanbaatr', 1, 'sukhbaatar', NULL, 50000, 60000, 'Pending', 1580803447, NULL, 'Enebish Dashzeveg', 'enegee@yahoo.com', '+97699289935', 1581408247, 'PAID'),
(1289, 59, 1, 16, 'Khansville, 103 bair, 1 orts, 1401 toot', 'Khan-Uul', 'Ulaanbaatar', 13313, 'UB', NULL, 50000, 195000, 'Ship', 1580358273, 1580576398, 'Bujinlkham  Ishjamts', 'bujinlkham.ishjamts@yahoo.com', '99092848', 1580724981, 'PAID'),
(1287, 42, 1, 5, 'Их Монгол Гудамж, 121', 'Баянзүрх дүүрэг, 26-р Хороо', 'Улаанбаатар', 13312, 'Улаанбаатар', NULL, 50000, 30000, 'Pending', 1579151106, NULL, 'Ankhbayar Batbaatar', 'ankhbayar@gmail.com', '99995019', 1579755906, 'NOT PAID'),
(1288, 45, 1, 3, '13 байр 4 орц 233', 'баянзүрх 26р хороо', 'улаанбаатар', 976, 'улаанбаатар', NULL, 50000, 0, 'Pending', 1580010189, NULL, 'ganbat ganbat', 'khatanjargal1212@gmail.com', '88901971', 1580614989, 'NOT PAID');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_setting`
--

CREATE TABLE `tb_order_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `minimum_order_quantity` int(10) DEFAULT NULL,
  `minimum_order_pricing` int(10) DEFAULT NULL,
  `shipping_cost` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_order_setting`
--

INSERT INTO `tb_order_setting` (`id`, `minimum_order_quantity`, `minimum_order_pricing`, `shipping_cost`) VALUES
(1, 19, 123, 4324);

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment`
--

CREATE TABLE `tb_payment` (
  `payment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'Not Paid'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_payment`
--

INSERT INTO `tb_payment` (`payment_id`, `order_id`, `status`) VALUES
(1, 1086, 'PAID'),
(2, 1087, 'PAID'),
(3, 1088, 'PAID'),
(4, 1089, 'PAID'),
(5, 1090, 'PAID'),
(6, 1091, 'PAID'),
(7, 1105, 'PAID'),
(8, 1108, 'PAID'),
(9, 1114, 'PAID'),
(10, 1121, 'PAID'),
(11, 1129, 'PAID'),
(12, 1130, 'PAID'),
(13, 1131, 'PAID'),
(14, 1132, 'PAID'),
(15, 1133, 'PAID'),
(16, 1134, 'PAID'),
(17, 1135, 'PAID'),
(18, 1137, 'PAID'),
(19, 1140, 'PAID'),
(20, 1142, 'PAID'),
(21, 1143, 'PAID'),
(22, 1144, 'PAID'),
(23, 1145, 'PAID'),
(24, 1146, 'PAID'),
(25, 1147, 'PAID'),
(26, 1148, 'PAID'),
(27, 1150, 'PAID'),
(28, 1153, 'PAID'),
(29, 1154, 'PAID'),
(30, 1155, 'PAID'),
(31, 1156, 'PAID'),
(32, 1240, 'PAID'),
(33, 1242, 'PAID'),
(34, 1246, 'PAID'),
(35, 1248, 'PAID'),
(36, 1255, 'PAID'),
(37, 1260, 'PAID'),
(38, 1261, 'PAID'),
(39, 1262, 'PAID'),
(40, 1273, 'PAID'),
(41, 1275, 'PAID'),
(42, 1278, 'PAID'),
(43, 1279, 'PAID'),
(44, 1280, 'PAID'),
(45, 1290, 'PAID');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pixil`
--

CREATE TABLE `tb_pixil` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pixil`
--

INSERT INTO `tb_pixil` (`id`, `order_id`, `url`) VALUES
(977, 1262, 'avatar/pixils215789931681.png'),
(976, 1262, 'avatar/pixils115789931681.png'),
(975, 1262, 'avatar/pixils015789931681.png'),
(1082, 1290, 'avatar/pixils615808034472.png'),
(1081, 1290, 'avatar/pixils515808034472.png'),
(1080, 1290, 'avatar/pixils415808034472.png'),
(1079, 1290, 'avatar/pixils315808034472.png'),
(1078, 1290, 'avatar/pixils215808034472.png'),
(1077, 1290, 'avatar/pixils115808034472.png'),
(1076, 1290, 'avatar/pixils015808034471.png'),
(1075, 1289, 'avatar/pixils1515803582791.png'),
(1074, 1289, 'avatar/pixils1415803582791.png'),
(1073, 1289, 'avatar/pixils1315803582791.png'),
(1072, 1289, 'avatar/pixils1215803582791.png'),
(1071, 1289, 'avatar/pixils1115803582791.png'),
(1070, 1289, 'avatar/pixils1015803582791.png'),
(1069, 1289, 'avatar/pixils915803582750.png'),
(1068, 1289, 'avatar/pixils815803582750.png'),
(1067, 1289, 'avatar/pixils715803582750.png'),
(1066, 1289, 'avatar/pixils615803582750.png'),
(1065, 1289, 'avatar/pixils515803582750.png'),
(1064, 1289, 'avatar/pixils415803582734.png'),
(1063, 1289, 'avatar/pixils315803582734.png'),
(1062, 1289, 'avatar/pixils215803582734.png'),
(1061, 1289, 'avatar/pixils115803582734.png'),
(1060, 1289, 'avatar/pixils015803582733.png'),
(1059, 1288, 'avatar/pixils215800101895.png'),
(1058, 1288, 'avatar/pixils115800101895.png'),
(1057, 1288, 'avatar/pixils015800101895.png'),
(1056, 1287, 'avatar/pixils415791511062.png'),
(1055, 1287, 'avatar/pixils315791511062.png'),
(1054, 1287, 'avatar/pixils215791511062.png'),
(1053, 1287, 'avatar/pixils115791511062.png'),
(1052, 1287, 'avatar/pixils015791511061.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL,
  `url_front` varchar(200) NOT NULL,
  `name_mong` varchar(200) NOT NULL,
  `name_eng` varchar(200) NOT NULL,
  `photo_size_width` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `photo_size_height` int(10) NOT NULL,
  `frame_size_width` int(10) NOT NULL,
  `frame_size_height` int(10) NOT NULL,
  `url_right` varchar(255) NOT NULL,
  `url_bottom` varchar(255) NOT NULL,
  `product_size_width` int(10) NOT NULL,
  `product_size_height` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`id`, `url_front`, `name_mong`, `name_eng`, `photo_size_width`, `price`, `photo_size_height`, `frame_size_width`, `frame_size_height`, `url_right`, `url_bottom`, `product_size_width`, `product_size_height`) VALUES
(1, 'avatar/products/1569757797.png', 'Modern', 'Modern', 703, 15000, 703, 703, 703, 'avatar/products/15697577971.png', 'avatar/products/15697577972.png', 20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tb_promo`
--

CREATE TABLE `tb_promo` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `off` int(10) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `allowed` int(10) DEFAULT NULL,
  `redeemed` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_promo`
--

INSERT INTO `tb_promo` (`id`, `code`, `off`, `start_date`, `end_date`, `allowed`, `redeemed`) VALUES
(1, 'FALLNIGHT2019', 30, '2019-10-06 04:25:22', '2019-11-01 04:25:22', 12, 0),
(2, '1234', 12, '2019-11-04 08:05:58', '2019-11-11 00:45:58', 6, 0),
(5, '12345', 12, '2019-11-05 05:30:59', '2019-11-27 13:30:59', 100, 0),
(6, 'TEST123', 20, '2019-12-06 07:00:40', '2019-12-09 06:55:40', 30, 0),
(7, 'TEST999', 10, '2019-12-14 07:00:54', '2020-01-16 20:00:54', 10, 0),
(8, 'Jan2020', 20, '2020-01-06 17:05:14', '2020-01-10 18:05:14', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_shipaddress`
--

CREATE TABLE `tb_shipaddress` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_shipaddress`
--

INSERT INTO `tb_shipaddress` (`id`, `user_id`, `address`, `district`, `city`, `zip_code`, `province`) VALUES
(28, 72, '229-118', 'sukhbaatar 9 xoroo dolgoonnuur', 'Ulaanbaatr', '1', 'sukhbaatar'),
(29, 27, 'test', 'test', 'gg', '123', 'test'),
(27, 27, 'yanji', 'Jilin', 'yanj', '133000', 'Jilin '),
(26, 59, 'Khansville, 103 bair, 1 orts, 1401 toot', 'Khan-Uul', 'Ulaanbaatar', '13313', 'UB'),
(24, 42, 'Их Монгол Гудамж, 121', 'Баянзүрх дүүрэг, 26-р Хороо', 'Улаанбаатар', '13312', 'Улаанбаатар'),
(22, 39, 'Khans', '15', 'Ub', '0', 'mongol'),
(25, 45, '13 байр 4 орц 233', 'баянзүрх 26р хороо', 'улаанбаатар', '976', 'улаанбаатар'),
(20, 37, 'Бага тойрог, 5-р байр, 2 орц, 9 тоот', 'Чингэлтэй дүүрэг, 1-р хороо', 'Улаанбаатар', '80111', 'Улаанбаатар'),
(21, 37, 'Address 1', 'Address 2', 'City', '80999', 'UB city');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `u_id` int(11) NOT NULL,
  `u_firstname` varchar(200) NOT NULL,
  `u_lastname` varchar(200) NOT NULL,
  `u_email` varchar(200) NOT NULL,
  `u_phone` varchar(100) NOT NULL,
  `u_password` varchar(500) NOT NULL,
  `last_date` date NOT NULL,
  `reg_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`u_id`, `u_firstname`, `u_lastname`, `u_email`, `u_phone`, `u_password`, `last_date`, `reg_date`) VALUES
(51, 'altanbagana', 'b', 'altanbagana@judcouncil.mn', '99199693', '40e5e16fe48f9dd1cf7853da50b3ed35', '2020-01-30', '2020-01-23'),
(50, 'Ariunzaya', 'Bayaraa', 'ariunzayabayaraa@gmail.com', '89012324', '889ba416c4aca5d13d2bd76b138dfb94', '2020-02-02', '2020-01-22'),
(45, 'ganbat', 'ganbat', 'khatanjargal1212@gmail.com', '88901971', '337ad844c3469e7e84482ebea35a363c', '2020-01-29', '2020-01-20'),
(46, 'Анхбаяр', 'Ганзориг', 'g.anhaa63@gmail.com', '99701022', 'ceea0653a15b211faa218061747711a3', '0000-00-00', '2020-01-20'),
(47, 'Gantulga', 'Tsevegjav', 'gogantulga@gmail.com', '99663636', '72107980008ffa1727811ed713785195', '0000-00-00', '2020-01-21'),
(48, 'Narantuya', 'Gombosuren', 'narantuya_m@yahoo.co.nz', '99289695', '05f2d9b480bed280e891db38e3fdf151', '0000-00-00', '2020-01-21'),
(49, 'amartovshin', 'terbish', 'amaraatb@gmail.com', '+197699003445', '7e805f18f942a9857bd22f92aaf434dc', '2020-02-06', '2020-01-22'),
(42, 'Ankhbayar', 'Batbaatar', 'ankhbayar@gmail.com', '99995019', '34819d7beeabb9260a5c854bc85b3e44', '2020-02-05', '2020-01-15'),
(37, 'Test', 'User', 'ankhamax@gmail.com', '99999999', '34819d7beeabb9260a5c854bc85b3e44', '2020-01-15', '2019-12-13'),
(43, 'John', 'Doe', 'playstorecnx13@gmail.com', '6364908625', 'e807f1fcf82d132f9bb018ca6738a19f', '0000-00-00', '2020-01-19'),
(39, 'Galtbolor', 'Khishigbayar', 'galtbolor@yahoo.com', '99178190', '827ccb0eea8a706c4c34a16891f84e7b', '2020-01-30', '2020-01-09'),
(52, 'Ариунболд', 'Цэрэннадмид', 'ariuka9898@yahoo.com', '89861217', '29207cc37b90156e645d9ff0a5476600', '2020-01-31', '2020-01-23'),
(53, 'Туяажаргал ', 'Буянбадрах', 'b.tuyajargal@yahoo.com', '88029320', '23f12109669d171330a8efef8ebd15a1', '2020-01-25', '2020-01-25'),
(54, 'Энхбат', 'Төмөрбаатар', 'enhee2210@gmail.com', '90082210', '149815eb972b3c370dee3b89d645ae14', '0000-00-00', '2020-01-26'),
(55, 'пүрэвсанаа', 'төртогтох', 'pvrebsanaa0721@gmail.com', '88101954', '5f1a0d66fde30f4d2587496005119d5e', '2020-01-29', '2020-01-28'),
(56, 'ganpurev', 'myangandash', 'Ganpurev9911@gmail.com', '80806433', '8372643a2f0fbc8b746dc8a512509b9a', '2020-01-29', '2020-01-29'),
(57, 'Энхбаатар ', 'Мөнхтамир', 'taamiraa@gmail.com', '88887220', '059d40723bd69ea0856667d4988f14d1', '0000-00-00', '2020-01-29'),
(58, 'Гантулга', 'Базарррагчаа', 'Tuki_9899@yahoo.com', '88056217', '436b1c6532a34faeea9730b07899d110', '2020-02-02', '2020-01-30'),
(59, 'Bujinlkham ', 'Ishjamts', 'bujinlkham.ishjamts@yahoo.com', '99092848', '605b3386cb5ef25b8d60d1bacfeeab7a', '2020-02-02', '2020-01-30'),
(60, 'Temuulin', 'Enkhtuya', 'temuulin.e@esto.mn', '88112748', '8d5d45cacbf3074854187dbae882e90f', '2020-02-01', '2020-01-30'),
(61, 'tuguldur', 'OYUNBILEG', 'oyunbileg.tuguldur@yahoo.com', '99740985', 'c8a66fd432f307336c181ea84d4629c1', '2020-01-30', '2020-01-30'),
(62, 'aza', 'd', 'aza315@yahoo.com', '99058657', '0ae68ab9403c32b08ea5b0588cfb5f1e', '0000-00-00', '2020-01-31'),
(63, 'ГАНЗОРИГ', 'Лхагвасүрэн', 'ayud0560@yahoo.com', '88100560', '7bbfe1a435823826517a53a463cc33b9', '2020-01-31', '2020-01-31'),
(64, 'Гантулга', 'Пүрэв', 'ggk00278@gmail.com', '99287273', '8be755bb7698b3297ed7c567845e88dd', '2020-02-01', '2020-01-31'),
(65, 'Khangaikhuu', 'Uvgunkhuu', 'khangaikhuu@gmail.com', '97122789', '211021d2b119d78fe0e0d4d29eeff687', '0000-00-00', '2020-02-01'),
(66, 'Сонинбилэг', 'Лхагвацэрэн', 'bilgeetseren18@yahoo.com', '99269614', 'd5e27e6d95a97470b3e04e27da438169', '2020-02-02', '2020-02-02'),
(67, 'Анхбаяр', 'Батбаатар', 'pixilmn@gmail.com', '99415039', '25f9e794323b453885f5181f1b624d0b', '0000-00-00', '2020-02-03'),
(68, 'Нандинбаатар ', 'Алтанхуяг', 'naba_9992001@yahoo.com', '99195466', '3516c528bab6ce642770f1267010a298', '2020-02-04', '2020-02-03'),
(69, 'solongo', 'myagmar', 'msolonga1228@gmail.com', '88012867', 'ab04931d0dbb57bc08c8b33b98b4e8dc', '0000-00-00', '2020-02-03'),
(70, 'Болорцэцэг', 'Цолмон', 'bolortsetseg0803@gmail.com', '99092532', '516e3f74b2147ee8282623dc9e5ce255', '0000-00-00', '2020-02-04'),
(71, 'Энхтөр', 'Энхбат', 'enxtur.mon@gmail.com', '99585821', '7150489c0e4e7f882bc6a75a2ea377d3', '2020-02-04', '2020-02-04'),
(72, 'Enebish', 'Dashzeveg', 'enegee@yahoo.com', '+97699289935', 'e12bee3c0be29691960fdd6df35ccaff', '2020-02-04', '2020-02-04'),
(73, 'Tuul', 'Gan', 'altka_hey@yahoo.com', '98028494', '29a1fa4dd664da470bdec78e597a8951', '2020-02-04', '2020-02-04'),
(74, 'Өрнөх', 'Доржсүрэн', 'ganaaurnuu1007@gmail.com', '88737693', 'f6c9c801b168f2257bee4c0fe36aa298', '2020-02-04', '2020-02-04'),
(75, 'Boldbaatar', 'Sanduijav', 'boogii_esu7679@yahoo.com', '91107679', '333943ff8a14617d66ea94ec176fc787', '0000-00-00', '2020-02-05'),
(76, 'Khongorzul', 'Tsogbadrakh', 'galowsraven@gmail.com', '80844422', 'bca9e64bf5ab44f601f03d4e083780a8', '2020-02-06', '2020-02-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_app_setting`
--
ALTER TABLE `tb_app_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_media`
--
ALTER TABLE `tb_media`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_order_setting`
--
ALTER TABLE `tb_order_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_payment`
--
ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tb_pixil`
--
ALTER TABLE `tb_pixil`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_promo`
--
ALTER TABLE `tb_promo`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_shipaddress`
--
ALTER TABLE `tb_shipaddress`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`u_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `tb_app_setting`
--
ALTER TABLE `tb_app_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_media`
--
ALTER TABLE `tb_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1291;

--
-- AUTO_INCREMENT for table `tb_order_setting`
--
ALTER TABLE `tb_order_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_payment`
--
ALTER TABLE `tb_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tb_pixil`
--
ALTER TABLE `tb_pixil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1083;

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_promo`
--
ALTER TABLE `tb_promo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_shipaddress`
--
ALTER TABLE `tb_shipaddress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
